package com.syncmed.doc360_doctor_android.interfaces;

import com.syncmed.doc360_doctor_android.models.ClinicScheduleItem;
import com.syncmed.doc360_doctor_android.models.ClinicScheduleList;
import com.syncmed.doc360_doctor_android.models.DoctorBillingInfoData;
import com.syncmed.doc360_doctor_android.models.DoctorPracticeInfoData;
import com.syncmed.doc360_doctor_android.models.DoctorProfessionalInfoData;
import com.syncmed.doc360_doctor_android.models.DoctorProfileData;
import com.syncmed.doc360_doctor_android.models.SpecializationItem;

import java.util.List;

/**
 * Created by ernestgayyed on 28/09/2017.
 */

public interface RegistrationContract {
    interface View extends BaseView {
        void loadspecializationSearchList(List<SpecializationItem> specializationItemList);
        void loadSpecializationList(List<SpecializationItem> specializationItemList);
        void clearSearchList();
        void loadClinicSchedule(ClinicScheduleList scheduleList);
        void loadDoctorProfileData(DoctorProfileData doctorProfileData);
        void presentDoctorProfessionalInfo(DoctorProfessionalInfoData data);
        void presentDoctorBillingInfo(DoctorBillingInfoData data);
        void presentDoctorPracticeInfo(DoctorPracticeInfoData data);
        void presentHomeView();
    }

    interface DialogView extends BaseView {
        void dismissDialog();
        void saveSchedule(ClinicScheduleItem clinicScheduleItem);
    }

    interface DialogPresenter extends BasePresenter {

    }

    interface Presenter extends BasePresenter {

    }

    interface ItemListener {
        void onItemClicked(int position);
        void onItemEdited(int position);
        void onItemDeleted(int position);
    }

    interface BillingInfoListener {
        void onBillingDataSaved(DoctorBillingInfoData data);
    }

    interface DoctorRateListener {
        void onRateSaved(String voiceVideoRate, String chatRate);
    }

    interface DoctorScheduleListener {
        void onScheduleSaved(ClinicScheduleItem scheduleItem);
    }
}
