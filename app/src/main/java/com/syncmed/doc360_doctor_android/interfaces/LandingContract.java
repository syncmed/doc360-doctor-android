package com.syncmed.doc360_doctor_android.interfaces;

import com.syncmed.doc360_doctor_android.models.CareTeamItem;
import com.syncmed.doc360_doctor_android.models.ConsultItem;
import com.syncmed.doc360_doctor_android.models.ConsultRequest;
import com.syncmed.doc360_doctor_android.models.DoctorProfessionalInfoData;

import java.util.List;

/**
 * Created by ernestgayyed on 29/09/2017.
 */

public interface LandingContract {
    interface View extends BaseView {
        void loadDoctorDetails();
        void setupUserDetailsView(DoctorProfessionalInfoData doctorProfessionalInfoData);
        void loadConsultSchedules(List<ConsultItem> consultItemList);
        void loadPatientRequests(List<CareTeamItem> careTeamItems);
        void loadConsultRequests(List<ConsultRequest> consultRequests);
    }

    interface Presenter extends BasePresenter {

    }
}
