package com.syncmed.doc360_doctor_android.interfaces;

/**
 * Created by ernestgayyed on 29/09/2017.
 */

public interface RegisterContract {
    interface View extends BaseView {
        void presentRegistrationView();
        void presentHomeView();
        void registerFirebase(String email, String password);
        void loginFirebase(String email, String password);
    }

    interface Presenter extends BasePresenter {

    }
}
