package com.syncmed.doc360_doctor_android.interfaces;

import com.syncmed.doc360_doctor_android.models.CareTeamItem;

import java.util.List;

/**
 * Created by ernestgayyed on 29/09/2017.
 */

public interface PatientContract {
    interface View extends BaseView {
        void loadPatientList(List<CareTeamItem> patientList);
        void reloadPatientRequestBadge(String count);
    }

    interface Presenter extends BasePresenter {

    }

    interface RequestView extends BaseView {
        void loadPatientRequestList(List<CareTeamItem> patientRequestList);
        void setPatientRequestSummary(List<CareTeamItem> patientRequestSummary);
    }

    interface RequestPresenter extends BasePresenter {

    }

    interface PatientRequestListener {
        void onPatientAccepted(int position);
        void onPatientDeclined(int position);
    }
}
