package com.syncmed.doc360_doctor_android.interfaces;

/**
 * Created by ernestgayyed on 28/09/2017.
 */

public interface BasePresenter {
    void onViewCreated();
    void refreshData();
}
