package com.syncmed.doc360_doctor_android.interfaces;

import com.syncmed.doc360_doctor_android.models.CareTeamItem;
import com.syncmed.doc360_doctor_android.models.ConsultRequest;
import com.syncmed.doc360_doctor_android.models.DoctorProfessionalInfoData;

import java.util.List;

/**
 * Created by ernestgayyed on 29/09/2017.
 */

public interface HomeContract {
    interface View extends BaseView {
        void setPatientRequestBadge(String text);
        void setConsultRequestBadge(String text);
        void setPatientRequest(List<CareTeamItem> careTeamItems);
        void setConsultRequest(List<ConsultRequest> consultRequests);
        void getDeviceToken();
    }

    interface Presenter extends BasePresenter {

    }
}
