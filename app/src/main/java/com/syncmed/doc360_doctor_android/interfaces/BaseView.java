package com.syncmed.doc360_doctor_android.interfaces;

/**
 * Created by ernestgayyed on 28/09/2017.
 */

public interface BaseView {
    void showLoadingView();
    void hideLoadingView();
    void showErrorMessage(String title, String message);
    void showSuccessMessage(String title, String message);
    void setupListAdapter();
    void extractBundleData();
}
