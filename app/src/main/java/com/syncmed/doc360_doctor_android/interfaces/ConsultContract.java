package com.syncmed.doc360_doctor_android.interfaces;

import com.syncmed.doc360_doctor_android.models.ConsultRequest;
import com.syncmed.doc360_doctor_android.models.Patient;

import java.util.List;

/**
 * Created by ernestgayyed on 29/09/2017.
 */

public interface ConsultContract {
    interface View extends BaseView {
        void loadConsultRequestList(List<ConsultRequest> list);
        void reloadConsultRequestBadge(String count);
    }

    interface Presenter extends BasePresenter {

    }
}
