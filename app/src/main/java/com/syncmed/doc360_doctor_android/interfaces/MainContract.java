package com.syncmed.doc360_doctor_android.interfaces;

/**
 * Created by ernestgayyed on 29/09/2017.
 */

public interface MainContract {
    interface View extends BaseView {
        void presentHomeView();
        void getDeviceToken();
    }

    interface Presenter extends BasePresenter {

    }
}
