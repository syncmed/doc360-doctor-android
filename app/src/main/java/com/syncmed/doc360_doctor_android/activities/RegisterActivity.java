package com.syncmed.doc360_doctor_android.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.syncmed.doc360_doctor_android.Doc360Application;
import com.syncmed.doc360_doctor_android.R;
import com.syncmed.doc360_doctor_android.components.DaggerRegisterComponent;
import com.syncmed.doc360_doctor_android.fragments.RegistrationFragment;
import com.syncmed.doc360_doctor_android.helpers.Utility;
import com.syncmed.doc360_doctor_android.interfaces.RegisterContract;
import com.syncmed.doc360_doctor_android.modules.RegisterModule;
import com.syncmed.doc360_doctor_android.modules.RegistrationModule;
import com.syncmed.doc360_doctor_android.presenters.RegisterPresenter;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class RegisterActivity extends AppCompatActivity implements RegisterContract.View {

    @BindView(R.id.back_button)
    ImageButton backButton;
    @BindView(R.id.email_text)
    EditText emailText;
    @BindView(R.id.password_text)
    EditText passwordText;
    @BindView(R.id.confirm_password_text)
    EditText confirmPasswordText;
    @BindView(R.id.terms_text)
    TextView termsText;
    @BindView(R.id.register_button)
    Button registerButton;

    @Inject
    RegisterPresenter presenter;

    private SweetAlertDialog pDialog;
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

        DaggerRegisterComponent.builder()
                .requestComponent(((Doc360Application) getApplicationContext()).getRequestComponent())
                .registerModule(new RegisterModule(this))
                .build().inject(this);

        firebaseAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void showLoadingView() {
        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
        pDialog.setTitleText("Registering...");
        pDialog.setCancelable(false);
        pDialog.show();
    }

    @Override
    public void hideLoadingView() {
        pDialog.dismiss();
    }

    @Override
    public void showErrorMessage(String title, String message) {
        new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                .setTitleText(title)
                .setContentText(message)
                .show();
    }

    @Override
    public void showSuccessMessage(String title, String message) {
        new SweetAlertDialog(this)
                .setTitleText(title)
                .setContentText(message)
                .show();
    }

    @Override
    public void setupListAdapter() {

    }

    @Override
    public void extractBundleData() {

    }

    @OnClick({R.id.back_button, R.id.register_button})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_button:
                finish();
                break;
            case R.id.register_button:
                String email = emailText.getText().toString();
                String password = passwordText.getText().toString();
                String cPassword = confirmPasswordText.getText().toString();

                presenter.onRegisterClicked(email, password, cPassword);
                break;
        }
    }

    @Override
    public void presentRegistrationView() {
        RegistrationFragment fragment = new RegistrationFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right, android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        fragmentTransaction.add(android.R.id.content, fragment, "RegistrationFragment");
        fragmentTransaction.addToBackStack("RegistrationFragment");
        fragmentTransaction.commit();
    }

    @Override
    public void presentHomeView() {

    }

    @Override
    public void registerFirebase(String email, String password) {
        firebaseAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                        } else {

                        }
                    }
                });
    }

    @Override
    public void loginFirebase(String email, String password) {
        firebaseAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                        } else {

                        }
                    }
                });
    }
}