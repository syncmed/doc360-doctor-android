package com.syncmed.doc360_doctor_android.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.google.firebase.iid.FirebaseInstanceId;
import com.syncmed.doc360_doctor_android.Doc360Application;
import com.syncmed.doc360_doctor_android.R;
import com.syncmed.doc360_doctor_android.components.DaggerLoginComponent;
import com.syncmed.doc360_doctor_android.fragments.RegistrationFragment;
import com.syncmed.doc360_doctor_android.interfaces.LoginContract;
import com.syncmed.doc360_doctor_android.modules.LoginModule;
import com.syncmed.doc360_doctor_android.presenters.LoginPresenter;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class LoginActivity extends AppCompatActivity implements LoginContract.View {

    @Inject
    LoginPresenter presenter;
    @BindView(R.id.email_text)
    EditText emailText;
    @BindView(R.id.password_text)
    EditText passwordText;
    @BindView(R.id.login_button)
    Button loginButton;

    private FirebaseAuth firebaseAuth;
    private SweetAlertDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(getIntent())
                .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                    @Override
                    public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                        // Get deep link from result (may be null if no link is found)
                        Log.d("TEST", "getDynamicLink:onFailure");
                        Uri deepLink = null;
                        if (pendingDynamicLinkData != null) {
                            deepLink = pendingDynamicLinkData.getLink();
                        }
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("TEST", "getDynamicLink:onFailure", e);
                    }
                });

        DaggerLoginComponent.builder()
                .requestComponent(((Doc360Application) getApplicationContext()).getRequestComponent())
                .loginModule(new LoginModule(this))
                .build().inject(this);


        firebaseAuth = FirebaseAuth.getInstance();

        presenter.onViewCreated();
    }

    @Override
    public void showLoadingView() {
        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
        pDialog.setTitleText("Logging in...");
        pDialog.setCancelable(false);
        pDialog.show();
    }

    @Override
    public void hideLoadingView() {
        pDialog.dismiss();
    }

    @Override
    public void showErrorMessage(String title, String message) {
        new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                .setTitleText(title)
                .setContentText(message)
                .show();
    }

    @Override
    public void showSuccessMessage(String title, String message) {
        new SweetAlertDialog(this)
                .setTitleText(title)
                .setContentText(message)
                .show();
    }

    @Override
    public void setupListAdapter() {

    }

    @Override
    public void extractBundleData() {

    }

    @Override
    public void presentRegistrationView() {
        RegistrationFragment fragment = new RegistrationFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right, android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        fragmentTransaction.add(android.R.id.content, fragment, "RegistrationFragment");
        fragmentTransaction.addToBackStack("RegistrationFragment");
        fragmentTransaction.commit();
    }

    @Override
    public void presentHomeView() {
        startActivity(new Intent(this, HomeActivity.class));
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Fragment fragment = getSupportFragmentManager().findFragmentByTag("RegistrationFragment");
        fragment.onActivityResult(requestCode, resultCode, data);
    }

    @OnClick({R.id.back_button, R.id.login_button})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.login_button:
                presenter.onLoginClicked(emailText.getText().toString(), passwordText.getText().toString());
                break;
            case R.id.back_button:
                finish();
                break;
        }

    }

    @Override
    public void registerFirebase(final String email, final String password) {
        firebaseAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d("test", "test");
                        } else {

                        }
                    }
                });
    }

    public void loginFirebase(String email, String password) {
        firebaseAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d("test", "test");
                        } else {
                            Log.d("test", "test");
                        }
                    }
                });
    }


}