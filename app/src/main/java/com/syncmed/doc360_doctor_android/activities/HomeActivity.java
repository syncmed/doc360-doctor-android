package com.syncmed.doc360_doctor_android.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ashokvarma.bottomnavigation.BottomNavigationBar;
import com.ashokvarma.bottomnavigation.BottomNavigationItem;
import com.ashokvarma.bottomnavigation.TextBadgeItem;
import com.google.firebase.iid.FirebaseInstanceId;
import com.syncmed.doc360_doctor_android.Doc360Application;
import com.syncmed.doc360_doctor_android.R;
import com.syncmed.doc360_doctor_android.components.DaggerHomeComponent;
import com.syncmed.doc360_doctor_android.fragments.CalendarFragment;
import com.syncmed.doc360_doctor_android.fragments.ConsultFragment;
import com.syncmed.doc360_doctor_android.fragments.LandingFragment;
import com.syncmed.doc360_doctor_android.fragments.MessageFragment;
import com.syncmed.doc360_doctor_android.fragments.PatientFragment;
import com.syncmed.doc360_doctor_android.fragments.RegistrationFragment;
import com.syncmed.doc360_doctor_android.interfaces.HomeContract;
import com.syncmed.doc360_doctor_android.models.CareTeamItem;
import com.syncmed.doc360_doctor_android.models.ConsultRequest;
import com.syncmed.doc360_doctor_android.modules.HomeModule;
import com.syncmed.doc360_doctor_android.presenters.HomePresenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends AppCompatActivity implements HomeContract.View {

    @Inject
    HomePresenter presenter;

    @BindView(R.id.bottom_navigation_bar)
    BottomNavigationBar bottomNavigationBar;
    @BindView(R.id.notification_bar)
    RelativeLayout notifView;
    @BindView(R.id.notification_message)
    TextView notifMessage;
    @BindView(R.id.close_notif)
    ImageButton closeNotif;

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            // Get extra data included in the Intent
            String message = intent.getStringExtra("message");
            showNotificationBar(message);
        }
    };

    LandingFragment landingFragment;
    CalendarFragment calendarFragment;
    PatientFragment patientFragment;
    MessageFragment messageFragment;

    TextBadgeItem patientRequestBadgeItem = new TextBadgeItem();
    TextBadgeItem consultRequestBadgeItem = new TextBadgeItem();

    private List<CareTeamItem> patientRequests = new ArrayList<>();
    private List<ConsultRequest> consultRequests = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        DaggerHomeComponent.builder()
                .requestComponent(((Doc360Application) getApplicationContext()).getRequestComponent())
                .homeModule(new HomeModule(this))
                .build().inject(this);

        presenter.onViewCreated();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(
                mMessageReceiver);
        super.onPause();
    }

    @Override
    protected void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(
                mMessageReceiver, new IntentFilter("FirstMovePushService"));
        super.onResume();
    }

    @Override
    public void showLoadingView() {

    }

    @Override
    public void hideLoadingView() {

    }

    @Override
    public void showErrorMessage(String title, String message) {

    }

    @Override
    public void showSuccessMessage(String title, String message) {

    }

    @Override
    public void setupListAdapter() {
        landingFragment = LandingFragment.newInstance("Home");
        calendarFragment = CalendarFragment.newInstance("Calendar");
        patientFragment = PatientFragment.newInstance("Patients");
        messageFragment = MessageFragment.newInstance("Message");


        bottomNavigationBar
                .addItem(new BottomNavigationItem(R.drawable.homeicon, "Home"))
                .addItem(new BottomNavigationItem(R.drawable.calendaricon, "Calendar"))
                .addItem(new BottomNavigationItem(R.drawable.patienticon, "Patients"))
                .addItem(new BottomNavigationItem(R.drawable.messagesicon, "Message"))
                .setInActiveColor("#FF42C2B5").setActiveColor("#F5F6F8")
                .setBarBackgroundColor("#2f847b")
                .setMode(BottomNavigationBar.MODE_SHIFTING)
                .initialise();

        bottomNavigationBar.setTabSelectedListener(new BottomNavigationBar.OnTabSelectedListener() {
            @Override
            public void onTabSelected(int position) {
                switch (position) {
                    case 0:
                        getSupportFragmentManager().beginTransaction().replace(R.id.home_activity_frag_container, landingFragment, "LandingFragment").addToBackStack("LandingFragment").commitAllowingStateLoss();
                        break;
                    case 1:
                        getSupportFragmentManager().beginTransaction().replace(R.id.home_activity_frag_container, calendarFragment).commitAllowingStateLoss();
                        break;
                    case 2:
                        getSupportFragmentManager().beginTransaction().replace(R.id.home_activity_frag_container, patientFragment).commitAllowingStateLoss();
                        break;
                    case 3:
                        getSupportFragmentManager().beginTransaction().replace(R.id.home_activity_frag_container, messageFragment).commitAllowingStateLoss();
                        break;
                    default:
                        getSupportFragmentManager().beginTransaction().replace(R.id.home_activity_frag_container, landingFragment).commitAllowingStateLoss();
                        break;
                }
            }

            @Override
            public void onTabUnselected(int position) {

            }

            @Override
            public void onTabReselected(int position) {

            }
        });

        getSupportFragmentManager().beginTransaction().replace(R.id.home_activity_frag_container, landingFragment, "LandingFragment").addToBackStack("LandingFragment").commitAllowingStateLoss();

        closeNotif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                notifView.setVisibility(View.GONE);
            }
        });

        notifView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomNavigationBar.selectTab(2);
            }
        });
    }

    @Override
    public void extractBundleData() {
    }

    @Override
    public void setPatientRequestBadge(String text) {
        if(text.equals("0")) {
            patientRequestBadgeItem.hide();
        } else {
            patientRequestBadgeItem.show();
            patientRequestBadgeItem.setText(text);
        }
    }

    @Override
    public void setConsultRequestBadge(String text) {
        if(text.equals("0")) {
            consultRequestBadgeItem.hide();
        } else {
            consultRequestBadgeItem.show();
            consultRequestBadgeItem.setText(text);
        }
    }

    public void showNotificationBar(String message) {
        presenter.loadConsultRequests();
        presenter.loadPatientRequests();

        notifView.setVisibility(View.VISIBLE);
        notifMessage.setText(message);

        notifView.postDelayed(new Runnable() {
            public void run() {
                notifView.setVisibility(View.GONE);
            }
        }, 10000);
    }

    @Override
    public void getDeviceToken() {
        try {
            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
            presenter.postDeviceToken(refreshedToken);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setPatientRequest(List<CareTeamItem> careTeamItems) {
        this.patientRequests = careTeamItems;

        LandingFragment fragment = (LandingFragment) getSupportFragmentManager().findFragmentByTag("LandingFragment");
        fragment.loadPatientRequests(patientRequests);
    }

    @Override
    public void setConsultRequest(List<ConsultRequest> consultRequests) {
        this.consultRequests = consultRequests;

        LandingFragment fragment = (LandingFragment) getSupportFragmentManager().findFragmentByTag("LandingFragment");
        fragment.loadConsultRequests(consultRequests);
    }

    public List<CareTeamItem> getPatientRequest() {
        return patientRequests;
    }

    public List<ConsultRequest> getConsultRequest() {
        return consultRequests;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Fragment fragment = getSupportFragmentManager().findFragmentByTag("RegistrationFragment");
        fragment.onActivityResult(requestCode, resultCode, data);
    }
}