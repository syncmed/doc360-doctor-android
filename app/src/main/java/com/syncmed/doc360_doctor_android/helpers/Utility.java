package com.syncmed.doc360_doctor_android.helpers;

import android.text.TextUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ernestgayyed on 10/08/2017.
 */

public class Utility {
    private static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
    private static final Pattern LETTERS_ONLY =
            Pattern.compile("[a-zA-Z]+", Pattern.CASE_INSENSITIVE);

    public static boolean isEmailValid(String email) {
        if (TextUtils.isEmpty(email)) {
            return false;
        } else {
            Matcher emailMatcher = VALID_EMAIL_ADDRESS_REGEX.matcher(email);
            return emailMatcher.find();
        }
    }

    public static boolean isDateValid(String date) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");
        df.setLenient(false);
        boolean valid = false;
        try {
            df.parse(date);
            return true;
        } catch (ParseException e) {
        } // valid will still be false

        return true;
    }

    public static boolean isStringOnly(String text) {
        return text.matches("[a-zA-Z]+");
    }

    public static boolean isStringEmptyOrNull(String text) {
        if (text == null || text == "" || text.equals("")) {
            return true;
        }

        return false;
    }


    public static boolean isNumbersOnly(String text) {
        return text.matches("[0-9]+");
    }

    public static boolean isPasswordValid(String password) {
        if (password.length() > 6) {
            return true;
        }

        return false;
    }
}
