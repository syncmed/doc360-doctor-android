package com.syncmed.doc360_doctor_android.helpers;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.syncmed.doc360_doctor_android.MainActivity;
import com.syncmed.doc360_doctor_android.R;
import com.syncmed.doc360_doctor_android.activities.HomeActivity;
import com.syncmed.doc360_doctor_android.models.NotificationBody;
import com.syncmed.doc360_doctor_android.models.NotificationResponse;

import org.json.JSONObject;

import java.util.Map;

/**
 * Created by ernestgayyed on 02/10/2017.
 */

public class Doc360MessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    LocalBroadcastManager localBroadcastManager;

    @Override
    public void onCreate() {
        super.onCreate();

        localBroadcastManager = LocalBroadcastManager.getInstance(this);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
//        if (remoteMessage.getData().size() > 0) {
//            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
//
//            if (/* Check if data needs to be processed by long running job */ true) {
//                // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
//                scheduleJob();
//            } else {
//                // Handle message within 10 seconds
//                handleNow();
//            }
//
//        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        sendNotification(remoteMessage.getNotification().getBody());
    }

    @Override
    public void onMessageSent(String s) {
        super.onMessageSent(s);
    }

    private void sendNotification(String messageBody) {
//        Gson gson = new Gson();
//        NotificationResponse notification = gson.fromJson(messageBody, NotificationResponse.class);
//        NotificationBody notificationBody = notification.getBody().get(0);
//        String content = notificationBody.getFname() + " " + notificationBody.getLname() + " is requesting for you.";
//
//        Intent intent = new Intent(this, HomeActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
//                PendingIntent.FLAG_ONE_SHOT);
//
//        String channelId = "1000";
//        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//        NotificationCompat.Builder notificationBuilder =
//                new NotificationCompat.Builder(this, channelId)
//                        .setSmallIcon(R.drawable.img_default_profile)
//                        .setContentTitle(notification.getTitle())
//                        .setContentText(messageBody)
//                        .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
//                        .setAutoCancel(true)
//                        .setSound(defaultSoundUri)
//                        .setContentIntent(pendingIntent);
//
//        NotificationManager notificationManager =
//                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//
//        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());


        Intent pushService = new Intent("FirstMovePushService");
        if(messageBody != null) {
            pushService.putExtra("message", messageBody);
        }

        localBroadcastManager.sendBroadcast(pushService);
    }
}
