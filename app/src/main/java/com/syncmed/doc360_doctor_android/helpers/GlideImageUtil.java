package com.syncmed.doc360_doctor_android.helpers;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import java.io.File;

/**
 * Created by ernestgayyed on 30/09/2017.
 */

public class GlideImageUtil {
    private Context context;

    public GlideImageUtil(Context context) {
        this.context = context;
    }

    public void setImage(Uri uri, ImageView imageView) {
        final Context _context = context;

        Glide
                .with(_context)
                .load(new File(uri.getPath()))
                .into(imageView);
    }

    public void setImage(String url, ImageView imageView) {
        final Context _context = context;

        Glide
                .with(_context)
                .load(url)
                .into(imageView);
    }

    public void setCircularImage(String imageUrl, int placeHolderId, ImageView imageView) {
        final Context _context = context;

        Glide
                .with(_context)
                .load(imageUrl)
                .apply(new RequestOptions()
                        .placeholder(placeHolderId))
                .into(imageView);
    }

    public void setCircularImage(Uri uri, int placeHolderId, ImageView imageView) {
        final Context _context = context;

        Glide
                .with(_context)
                .load(uri)
                .apply(new RequestOptions()
                        .placeholder(placeHolderId))
                .into(imageView);

//        Picasso.with(context)
//                .load(uri)
//                .placeholder(R.drawable.img_default_profile)
//                .into(imageView);
    }
}
