package com.syncmed.doc360_doctor_android.helpers;

import com.syncmed.doc360_doctor_android.models.AccessToken;
import com.syncmed.doc360_doctor_android.models.Account;
import com.syncmed.doc360_doctor_android.models.DoctorProfileData;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by ernestgayyed on 14/08/2017.
 */

public class DataManager {

    private Realm realm = Realm.getDefaultInstance();

    public void setAccessToken(AccessToken accessToken) {
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(accessToken);
        realm.commitTransaction();
    }

    public AccessToken getAccessToken() {
        return realm.where(AccessToken.class).findFirst();
    }

    public void setLoggedAccount(Account account) {
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(account);
        realm.commitTransaction();
    }

    public Account getLoggedAccount() {
        return realm.where(Account.class).notEqualTo("accountId", "0").findFirst();
    }

    public void setLoggedDoctorProfile(DoctorProfileData doctorProfile) {
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(doctorProfile);
        realm.commitTransaction();
    }

    public void removeLoggedDoctorProfile() {
        realm.beginTransaction();
        realm.delete(DoctorProfileData.class);
        realm.commitTransaction();
    }

    public DoctorProfileData getLoggedDoctorProfile() {
        return realm.where(DoctorProfileData.class).findFirst();
    }

}