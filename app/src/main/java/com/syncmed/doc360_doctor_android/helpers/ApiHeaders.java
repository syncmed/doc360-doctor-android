package com.syncmed.doc360_doctor_android.helpers;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by ernestgayyed on 11/08/2017.
 */

public class ApiHeaders implements Interceptor {
    private String authValue;

    public void clearAuthValue() {
        authValue = null;
    }

    public void setAuthValue(String authValue) {
        this.authValue = authValue;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        if(!Utility.isStringEmptyOrNull(authValue)) {
            Request newRequest  = chain.request().newBuilder()
                    .addHeader("Authorization", "Bearer " + authValue)
                    .build();
            return chain.proceed(newRequest);
        }

        return null;
    }
}