package com.syncmed.doc360_doctor_android;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.google.firebase.iid.FirebaseInstanceId;
import com.syncmed.doc360_doctor_android.activities.HomeActivity;
import com.syncmed.doc360_doctor_android.activities.LoginActivity;
import com.syncmed.doc360_doctor_android.activities.RegisterActivity;
import com.syncmed.doc360_doctor_android.components.DaggerMainComponent;
import com.syncmed.doc360_doctor_android.interfaces.MainContract;
import com.syncmed.doc360_doctor_android.modules.MainModule;
import com.syncmed.doc360_doctor_android.presenters.MainPresenter;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class MainActivity extends AppCompatActivity implements MainContract.View {


    @BindView(R.id.login_button)
    Button loginButton;
    @BindView(R.id.signup_button)
    Button signupButton;

    @Inject
    MainPresenter presenter;

    private SweetAlertDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DaggerMainComponent.builder()
                .requestComponent(((Doc360Application) getApplicationContext()).getRequestComponent())
                .mainModule(new MainModule(this))
                .build().inject(this);

        ButterKnife.bind(this);

        presenter.onViewCreated();
    }

    @OnClick({R.id.login_button, R.id.signup_button})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.login_button:
                startActivity(new Intent(this, LoginActivity.class));
                break;
            case R.id.signup_button:
                startActivity(new Intent(this, RegisterActivity.class));
                break;
        }
    }

    @Override
    public void showLoadingView() {
        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
        pDialog.setTitleText("Logging in...");
        pDialog.setCancelable(false);
        pDialog.show();
    }

    @Override
    public void hideLoadingView() {
        pDialog.dismiss();
    }

    @Override
    public void showErrorMessage(String title, String message) {

    }

    @Override
    public void showSuccessMessage(String title, String message) {

    }

    @Override
    public void setupListAdapter() {

    }

    @Override
    public void extractBundleData() {

    }

    @Override
    public void presentHomeView() {
        startActivity(new Intent(this, HomeActivity.class));
        finish();
    }

    @Override
    public void getDeviceToken() {
        try {
            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
            presenter.postDeviceToken(refreshedToken);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}