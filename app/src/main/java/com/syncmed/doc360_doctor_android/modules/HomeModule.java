package com.syncmed.doc360_doctor_android.modules;

import com.syncmed.doc360_doctor_android.interfaces.HomeContract;
import com.syncmed.doc360_doctor_android.interfaces.LoginContract;
import com.syncmed.doc360_doctor_android.scopes.Activity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by ernestgayyed on 28/09/2017.
 */
@Module
public class HomeModule {
    HomeContract.View view;

    public HomeModule(HomeContract.View view) {
        this.view = view;
    }

    @Provides
    @Activity
    public HomeContract.View provideView() {
        return view;
    }
}
