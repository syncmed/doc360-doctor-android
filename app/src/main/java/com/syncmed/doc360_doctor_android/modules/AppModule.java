package com.syncmed.doc360_doctor_android.modules;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.syncmed.doc360_doctor_android.Doc360Application;
import com.syncmed.doc360_doctor_android.R;
import com.syncmed.doc360_doctor_android.helpers.DataManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ernestgayyed on 10/08/2017.
 */

@Module
public class AppModule {
    private Doc360Application doc360Application;

    public AppModule(Doc360Application doc360Application) {
        this.doc360Application = doc360Application;
    }

    @Provides
    @Singleton
    Doc360Application provideDoc360Application() {
        return doc360Application;
    }

    @Provides
    @Singleton
    public DataManager provideDataManager() {
        return new DataManager();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit() {
        return new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(doc360Application.getString(R.string.host_url))
                .build();
    }
}