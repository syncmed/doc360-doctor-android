package com.syncmed.doc360_doctor_android.modules;

import com.syncmed.doc360_doctor_android.request.NetworkAPI;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by ernestgayyed on 10/08/2017.
 */
@Module
public class RequestModule {
    @Provides
    @Singleton
    public NetworkAPI provideNetworkRequest(Retrofit retrofit) {
        return retrofit.create(NetworkAPI.class);
    }
}