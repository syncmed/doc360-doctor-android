package com.syncmed.doc360_doctor_android.modules;

import com.syncmed.doc360_doctor_android.interfaces.RegistrationContract;
import com.syncmed.doc360_doctor_android.scopes.Activity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by ernestgayyed on 28/09/2017.
 */
@Module
public class RegistrationModule {
    RegistrationContract.View view;

    public RegistrationModule(RegistrationContract.View view) {
        this.view = view;
    }

    @Provides
    @Activity
    public RegistrationContract.View provideAllergiesContractView() {
        return view;
    }
}
