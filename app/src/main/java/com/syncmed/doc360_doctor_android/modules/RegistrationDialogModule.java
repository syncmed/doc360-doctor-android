package com.syncmed.doc360_doctor_android.modules;

import com.syncmed.doc360_doctor_android.interfaces.RegistrationContract;
import com.syncmed.doc360_doctor_android.scopes.Activity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by ernestgayyed on 28/09/2017.
 */
@Module
public class RegistrationDialogModule {
    RegistrationContract.DialogView view;

    public RegistrationDialogModule(RegistrationContract.DialogView view) {
        this.view = view;
    }

    @Provides
    @Activity
    public RegistrationContract.DialogView provideView() {
        return view;
    }
}
