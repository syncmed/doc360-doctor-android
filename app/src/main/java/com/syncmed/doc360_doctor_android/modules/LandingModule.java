package com.syncmed.doc360_doctor_android.modules;

import com.syncmed.doc360_doctor_android.interfaces.HomeContract;
import com.syncmed.doc360_doctor_android.interfaces.LandingContract;
import com.syncmed.doc360_doctor_android.scopes.Activity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by ernestgayyed on 28/09/2017.
 */
@Module
public class LandingModule {
    LandingContract.View view;

    public LandingModule(LandingContract.View view) {
        this.view = view;
    }

    @Provides
    @Activity
    public LandingContract.View provideView() {
        return view;
    }
}
