package com.syncmed.doc360_doctor_android.modules;

import com.syncmed.doc360_doctor_android.interfaces.PatientContract;
import com.syncmed.doc360_doctor_android.scopes.Activity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by ernestgayyed on 28/09/2017.
 */
@Module
public class PatientRequestModule {
    PatientContract.RequestView view;

    public PatientRequestModule(PatientContract.RequestView view) {
        this.view = view;
    }

    @Provides
    @Activity
    public PatientContract.RequestView provideView() {
        return view;
    }
}
