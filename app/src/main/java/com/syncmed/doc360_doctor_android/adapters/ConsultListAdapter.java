package com.syncmed.doc360_doctor_android.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.meg7.widget.CircleImageView;
import com.syncmed.doc360_doctor_android.R;
import com.syncmed.doc360_doctor_android.helpers.GlideImageUtil;
import com.syncmed.doc360_doctor_android.models.CareTeamItem;
import com.syncmed.doc360_doctor_android.models.ConsultRequest;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ernestgayyed on 28/09/2017.
 */

public class ConsultListAdapter extends RecyclerView.Adapter<ConsultListAdapter.ViewHolder> {

    private Context context;
    private List<ConsultRequest> list = new ArrayList();
    private List<ConsultRequest> searchList = new ArrayList();

    public void addItem(ConsultRequest item) {
        if (!list.contains(item)) {
            list.add(item);
            notifyItemInserted(list.size());
        }
    }

    public void removeItem(int position) {
        list.remove(position);
        notifyItemRemoved(position);
    }

    public void addList(List<ConsultRequest> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public void addSearchList(List<ConsultRequest> list) {
        this.searchList = list;
    }

    public List<ConsultRequest> getList() {
        return list;
    }

    public List<ConsultRequest> getSearchList() {
        return searchList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.context = parent.getContext();

        View itemView = LayoutInflater.from(context).inflate(R.layout.view_patient_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ConsultRequest item = list.get(position);
        holder.nameText.setText(item.getPatient().getFname() + " " + item.getPatient().getLname());

        GlideImageUtil glideImageUtil = new GlideImageUtil(context);
        glideImageUtil.setCircularImage(item.getPatient().getImageUrl(), R.drawable.img_default_profile, holder.userImage);
    }

    @Override
    public int getItemCount() {
        if(list.size() > 3) {
            return 3;
        } else {
            return list.size();
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.user_image)
        CircleImageView userImage;
        @BindView(R.id.name_text)
        TextView nameText;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
