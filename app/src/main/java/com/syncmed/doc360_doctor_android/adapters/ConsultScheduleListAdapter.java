package com.syncmed.doc360_doctor_android.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.syncmed.doc360_doctor_android.R;
import com.syncmed.doc360_doctor_android.models.ConsultItem;
import com.syncmed.doc360_doctor_android.models.Patient;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ernestgayyed on 15/08/2017.
 */

public class ConsultScheduleListAdapter extends RecyclerView.Adapter<ConsultScheduleListAdapter.ViewHolder> {

    private Context context;
    private List<ConsultItem> list = new ArrayList();

    public void addItem(ConsultItem item) {
        if (!list.contains(item)) {
            list.add(item);
            notifyItemInserted(list.size());
        }
    }

    public void removeItem(int position) {
        list.remove(position);
        notifyItemRemoved(position);
    }

    public void addList(List<ConsultItem> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.context = parent.getContext();

        View itemView = LayoutInflater.from(context).inflate(R.layout.view_consult_schedule_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Patient patient = list.get(position).getPatient();
        String schedule = list.get(position).getConsultSchedule();
        String type = list.get(position).getConsultType();

        String text = type + " with " + patient.getFname() + " " + patient.getLname();
        holder.itemText.setText(text);
        holder.subItemText.setText(schedule);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_text)
        TextView itemText;
        @BindView(R.id.sub_item_text)
        TextView subItemText;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
