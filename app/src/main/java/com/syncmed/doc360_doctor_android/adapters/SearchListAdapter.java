package com.syncmed.doc360_doctor_android.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.syncmed.doc360_doctor_android.R;
import com.syncmed.doc360_doctor_android.interfaces.RegistrationContract;
import com.syncmed.doc360_doctor_android.models.SpecializationItem;
import com.syncmed.doc360_doctor_android.models.SpecializationResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ernestgayyed on 28/09/2017.
 */

public class SearchListAdapter extends RecyclerView.Adapter<SearchListAdapter.ViewHolder> {

    private Context context;
    private List<SpecializationItem> list = new ArrayList();
    private RegistrationContract.ItemListener itemListener;

    public RegistrationContract.ItemListener getItemListener() {
        return itemListener;
    }

    public void setItemListener(RegistrationContract.ItemListener itemListener) {
        this.itemListener = itemListener;
    }

    public void addItem(SpecializationItem item) {
        if (!list.contains(item)) {
            list.add(item);
            notifyItemInserted(list.size());
        }
    }

    public void removeItem(int position) {
        list.remove(position);
        notifyItemRemoved(position);
    }

    public void addList(List<SpecializationItem> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public List<SpecializationItem> getList() {
        return list;
    }

    public void removeSearchList() {
        list.clear();
        notifyDataSetChanged();
    }

    @Override
    public SearchListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.context = parent.getContext();

        View itemView = LayoutInflater.from(context).inflate(R.layout.view_simple_search_item, parent, false);
        return new SearchListAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SearchListAdapter.ViewHolder holder, int position) {
        holder.itemText.setText(list.get(position).getDesc());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_text)
        TextView itemText;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemListener.onItemClicked(getAdapterPosition());
                }
            });
        }
    }
}
