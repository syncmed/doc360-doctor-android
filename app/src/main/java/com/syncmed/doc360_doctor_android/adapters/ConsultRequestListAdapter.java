package com.syncmed.doc360_doctor_android.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.meg7.widget.CircleImageView;
import com.syncmed.doc360_doctor_android.R;
import com.syncmed.doc360_doctor_android.helpers.GlideImageUtil;
import com.syncmed.doc360_doctor_android.models.ConsultRequest;
import com.syncmed.doc360_doctor_android.models.Patient;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ernestgayyed on 28/09/2017.
 */

public class ConsultRequestListAdapter extends RecyclerView.Adapter<ConsultRequestListAdapter.ViewHolder> {

    private Context context;
    private List<ConsultRequest> list = new ArrayList();

    public void addItem(ConsultRequest item) {
        if (!list.contains(item)) {
            list.add(item);
            notifyItemInserted(list.size());
        }
    }

    public void removeItem(int position) {
        list.remove(position);
        notifyItemRemoved(position);
    }

    public void addList(List<ConsultRequest> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public List<ConsultRequest> getList() {
        return list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.context = parent.getContext();

        View itemView = LayoutInflater.from(context).inflate(R.layout.view_patient_consult_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ConsultRequest item = list.get(position);
        Patient patient = item.getPatient();
        holder.nameText.setText(patient.getFname() + " " + patient.getLname());
        holder.consultTypeText.setText(item.getConsultType());
        holder.consultSchedText.setText(item.getConsultSchedule());

        GlideImageUtil glideImageUtil = new GlideImageUtil(context);
        glideImageUtil.setCircularImage(patient.getImageUrl(), R.drawable.img_default_profile, holder.userImage);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.user_image)
        CircleImageView userImage;
        @BindView(R.id.name_text)
        TextView nameText;
        @BindView(R.id.consult_type_text)
        TextView consultTypeText;
        @BindView(R.id.consult_sched_text)
        TextView consultSchedText;
        @BindView(R.id.accept_button)
        Button acceptButton;
        @BindView(R.id.resched_button)
        Button reschedButton;
        @BindView(R.id.visit_clinic_button)
        Button visitClinicButton;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            acceptButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });

            reschedButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });

            visitClinicButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });

        }
    }
}
