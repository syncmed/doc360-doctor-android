package com.syncmed.doc360_doctor_android.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.meg7.widget.CircleImageView;
import com.syncmed.doc360_doctor_android.R;
import com.syncmed.doc360_doctor_android.helpers.GlideImageUtil;
import com.syncmed.doc360_doctor_android.interfaces.PatientContract;
import com.syncmed.doc360_doctor_android.models.CareTeamItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ernestgayyed on 28/09/2017.
 */

public class PatientRequestListAdapter extends RecyclerView.Adapter<PatientRequestListAdapter.ViewHolder> {

    private Context context;
    private List<CareTeamItem> list = new ArrayList();
    private List<CareTeamItem> listCopy = new ArrayList<>();
    private PatientContract.PatientRequestListener patientRequestListener;

    public void addItem(CareTeamItem item) {
        if (!list.contains(item)) {
            list.add(item);
            notifyItemInserted(list.size());
        }
    }

    public void removeItem(int position) {
        list.remove(position);
        notifyItemRemoved(position);
    }

    public void addList(List<CareTeamItem> list) {
        this.list = list;
        listCopy.addAll(list);
        notifyDataSetChanged();
    }

    public void removeList() {
        list.clear();
        listCopy.clear();
        notifyDataSetChanged();
    }

    public List<CareTeamItem> getList() {
        return list;
    }

    public void filter(String text) {
        list.clear();
        if(text.isEmpty()){
            list.addAll(listCopy);
        } else{
            text = text.toLowerCase();
            for(CareTeamItem item: listCopy){
                String name = item.getFname() + " " + item.getLname();
                if(name.toLowerCase().contains(text)){
                    list.add(item);
                }
            }
        }
        notifyDataSetChanged();
    }

    public PatientContract.PatientRequestListener getPatientRequestListener() {
        return patientRequestListener;
    }

    public void setPatientRequestListener(PatientContract.PatientRequestListener patientRequestListener) {
        this.patientRequestListener = patientRequestListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.context = parent.getContext();

        View itemView = LayoutInflater.from(context).inflate(R.layout.view_patient_request_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        CareTeamItem item = list.get(position);
        holder.nameText.setText(item.getFname() + " " + item.getLname());

        GlideImageUtil glideImageUtil = new GlideImageUtil(context);
        glideImageUtil.setCircularImage(item.getPhotolink(), R.drawable.img_default_profile, holder.userImage);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.user_image)
        CircleImageView userImage;
        @BindView(R.id.name_text)
        TextView nameText;
        @BindView(R.id.accept_button)
        Button acceptButton;
        @BindView(R.id.decline_button)
        Button declineButton;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            acceptButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    patientRequestListener.onPatientAccepted(getAdapterPosition());
                }
            });

            declineButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    patientRequestListener.onPatientDeclined(getAdapterPosition());
                }
            });
        }
    }
}
