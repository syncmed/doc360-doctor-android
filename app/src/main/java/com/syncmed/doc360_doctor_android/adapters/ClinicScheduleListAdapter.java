package com.syncmed.doc360_doctor_android.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.syncmed.doc360_doctor_android.R;
import com.syncmed.doc360_doctor_android.interfaces.RegistrationContract;
import com.syncmed.doc360_doctor_android.models.ClinicScheduleItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ernestgayyed on 15/08/2017.
 */

public class ClinicScheduleListAdapter extends RecyclerView.Adapter<ClinicScheduleListAdapter.ViewHolder> {


    private Context context;
    private List<ClinicScheduleItem> list = new ArrayList();
    private RegistrationContract.ItemListener itemListener;

    public RegistrationContract.ItemListener getItemListener() {
        return itemListener;
    }

    public void setItemListener(RegistrationContract.ItemListener itemListener) {
        this.itemListener = itemListener;
    }

    public void addItem(ClinicScheduleItem item) {
        if (!list.contains(item)) {
            list.add(item);
            notifyItemInserted(list.size());
        }
    }

    public void removeItem(int position) {
        list.remove(position);
        notifyItemRemoved(position);
    }

    public void addList(List<ClinicScheduleItem> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public List<ClinicScheduleItem> getList() {
        return list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.context = parent.getContext();

        View itemView = LayoutInflater.from(context).inflate(R.layout.view_edit_delete_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ClinicScheduleItem clinicScheduleItem = list.get(position);
        holder.itemText.setText(clinicScheduleItem.getLocation());
        holder.subItemText.setText(clinicScheduleItem.getTime());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_text)
        TextView itemText;
        @BindView(R.id.sub_item_text)
        TextView subItemText;
        @BindView(R.id.edit_button)
        Button editButton;
        @BindView(R.id.delete_button)
        Button deleteButton;
        @BindView(R.id.action_layout)
        LinearLayout actionLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemListener.onItemDeleted(getAdapterPosition());
                    removeItem(getAdapterPosition());
                }
            });

            editButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemListener.onItemEdited(getAdapterPosition());
                }
            });
        }
    }
}
