package com.syncmed.doc360_doctor_android.models;

import io.realm.RealmObject;

/**
 * Created by ernestgayyed on 29/09/2017.
 */

public class Specialization extends RealmObject {
    private String specializationID;
    private String specialization;

    public String getSpecializationID() {
        return specializationID;
    }

    public void setSpecializationID(String specializationID) {
        this.specializationID = specializationID;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }
}
