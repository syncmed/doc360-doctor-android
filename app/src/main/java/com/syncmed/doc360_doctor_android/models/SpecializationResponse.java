package com.syncmed.doc360_doctor_android.models;

/**
 * Created by ernestgayyed on 28/09/2017.
 */

public class SpecializationResponse {
    private String status;
    private String code;
    private String message;
    private SpecializationData data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public SpecializationData getData() {
        return data;
    }

    public void setData(SpecializationData data) {
        this.data = data;
    }
}
