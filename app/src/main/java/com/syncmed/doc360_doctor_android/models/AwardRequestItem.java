package com.syncmed.doc360_doctor_android.models;

import java.util.List;

/**
 * Created by ernestgayyed on 30/09/2017.
 */

public class AwardRequestItem {
    private List<String> award;

    public List<String> getaward() {
        return award;
    }

    public void setaward(List<String> award) {
        this.award = award;
    }
}
