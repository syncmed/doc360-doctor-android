package com.syncmed.doc360_doctor_android.models;

/**
 * Created by ernestgayyed on 29/09/2017.
 */

public class LoginRequest {
    private String email;
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
