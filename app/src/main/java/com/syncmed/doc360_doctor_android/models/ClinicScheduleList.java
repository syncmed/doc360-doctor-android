package com.syncmed.doc360_doctor_android.models;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by ernestgayyed on 29/09/2017.
 */

public class ClinicScheduleList extends RealmObject {
    private RealmList<ClinicScheduleItem> monday;
    private RealmList<ClinicScheduleItem> tuesday;
    private RealmList<ClinicScheduleItem> wednesday;
    private RealmList<ClinicScheduleItem> thursday;
    private RealmList<ClinicScheduleItem> friday;

    public List<ClinicScheduleItem> getMonday() {
        return monday;
    }

    public void setMonday(RealmList<ClinicScheduleItem> monday) {
        this.monday = monday;
    }

    public List<ClinicScheduleItem> getTuesday() {
        return tuesday;
    }

    public void setTuesday(RealmList<ClinicScheduleItem> tuesday) {
        this.tuesday = tuesday;
    }

    public List<ClinicScheduleItem> getWednesday() {
        return wednesday;
    }

    public void setWednesday(RealmList<ClinicScheduleItem> wednesday) {
        this.wednesday = wednesday;
    }

    public List<ClinicScheduleItem> getThursday() {
        return thursday;
    }

    public void setThursday(RealmList<ClinicScheduleItem> thursday) {
        this.thursday = thursday;
    }

    public List<ClinicScheduleItem> getFriday() {
        return friday;
    }

    public void setFriday(RealmList<ClinicScheduleItem> friday) {
        this.friday = friday;
    }
}
