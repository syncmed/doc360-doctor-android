package com.syncmed.doc360_doctor_android.models;

/**
 * Created by ernestgayyed on 03/10/2017.
 */

public class DeviceToken {
    private String deviceToken;

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }
}
