package com.syncmed.doc360_doctor_android.models;

/**
 * Created by ernestgayyed on 29/09/2017.
 */

public class LoginResponseData {
    private String access_token;
    private String userID;
    private String userType;
    private String distinctID;

    public String getAccessToken() {
        return access_token;
    }

    public void setAccessToken(String accessToken) {
        this.access_token = accessToken;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getDistinctID() {
        return distinctID;
    }

    public void setDistinctID(String distinctID) {
        this.distinctID = distinctID;
    }
}
