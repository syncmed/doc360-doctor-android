package com.syncmed.doc360_doctor_android.models;

import java.util.List;

/**
 * Created by ernestgayyed on 02/10/2017.
 */

public class CareTeamPendingResponse {
    private String status;
    private String code;
    private String message;
    private List<CareTeamItem> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<CareTeamItem> getData() {
        return data;
    }

    public void setData(List<CareTeamItem> data) {
        this.data = data;
    }
}
