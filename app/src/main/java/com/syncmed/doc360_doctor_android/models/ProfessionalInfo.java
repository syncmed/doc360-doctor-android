package com.syncmed.doc360_doctor_android.models;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;

/**
 * Created by ernestgayyed on 29/09/2017.
 */

public class ProfessionalInfo extends RealmObject {

    private String id;
    @Ignore
    private List<String> education;
    @Ignore
    private List<String> certification;
    @Ignore
    private List<String> award;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getEducation() {
        return education;
    }

    public void setEducation(List<String> education) {
        this.education = education;
    }

    public List<String> getCertification() {
        return certification;
    }

    public void setCertification(List<String> certification) {
        this.certification = certification;
    }

    public List<String> getAward() {
        return award;
    }

    public void setAward(List<String> award) {
        this.award = award;
    }
}
