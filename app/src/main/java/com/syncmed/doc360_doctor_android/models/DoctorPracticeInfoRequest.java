package com.syncmed.doc360_doctor_android.models;

import java.util.List;

/**
 * Created by ernestgayyed on 30/09/2017.
 */

public class DoctorPracticeInfoRequest {
    private List<DoctorPracticeInfoData> doctordetails;

    public List<DoctorPracticeInfoData> getDoctordetails() {
        return doctordetails;
    }

    public void setDoctordetails(List<DoctorPracticeInfoData> doctordetails) {
        this.doctordetails = doctordetails;
    }
}
