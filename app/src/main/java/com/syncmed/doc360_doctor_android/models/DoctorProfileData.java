package com.syncmed.doc360_doctor_android.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by ernestgayyed on 29/09/2017.
 */

public class DoctorProfileData extends RealmObject {

    @PrimaryKey
    private String id;
    private DoctorProfessionalInfoData professionalInfo;
    private DoctorBillingInfoData billInfo;
    private DoctorPracticeInfoData practiceInfo;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DoctorProfessionalInfoData getProfessionalInfo() {
        return professionalInfo;
    }

    public void setProfessionalInfo(DoctorProfessionalInfoData professionalInfo) {
        this.professionalInfo = professionalInfo;
    }

    public DoctorBillingInfoData getBillInfo() {
        return billInfo;
    }

    public void setBillInfo(DoctorBillingInfoData billInfo) {
        this.billInfo = billInfo;
    }

    public DoctorPracticeInfoData getPracticeInfo() {
        return practiceInfo;
    }

    public void setPracticeInfo(DoctorPracticeInfoData practiceInfo) {
        this.practiceInfo = practiceInfo;
    }
}
