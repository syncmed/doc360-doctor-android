package com.syncmed.doc360_doctor_android.models;

import java.util.List;

/**
 * Created by ernestgayyed on 30/09/2017.
 */

public class DoctorDetailRequest {
    private List<DoctorDetailsDataRequest> doctordetails;

    public List<DoctorDetailsDataRequest> getDoctordetails() {
        return doctordetails;
    }

    public void setDoctordetails(List<DoctorDetailsDataRequest> doctordetails) {
        this.doctordetails = doctordetails;
    }
}
