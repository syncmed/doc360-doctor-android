package com.syncmed.doc360_doctor_android.models;

import java.util.List;

/**
 * Created by ernestgayyed on 29/09/2017.
 */

public class DoctorProfileResponse {
    private String status;
    private String code;
    private String message;
    private List<DoctorProfileData> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DoctorProfileData> getData() {
        return data;
    }

    public void setData(List<DoctorProfileData> data) {
        this.data = data;
    }
}
