package com.syncmed.doc360_doctor_android.models;

import java.util.List;

/**
 * Created by ernestgayyed on 02/10/2017.
 */

public class PatientApprovalRequest {
    private List<PatientApprovalItem> careteam;

    public List<PatientApprovalItem> getCareteam() {
        return careteam;
    }

    public void setCareteam(List<PatientApprovalItem> careteam) {
        this.careteam = careteam;
    }
}
