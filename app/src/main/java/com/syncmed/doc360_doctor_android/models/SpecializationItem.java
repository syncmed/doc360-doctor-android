package com.syncmed.doc360_doctor_android.models;

/**
 * Created by ernestgayyed on 28/09/2017.
 */

public class SpecializationItem {
    private String id;
    private String desc;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
