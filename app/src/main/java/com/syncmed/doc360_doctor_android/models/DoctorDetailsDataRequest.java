package com.syncmed.doc360_doctor_android.models;

import java.util.List;

/**
 * Created by ernestgayyed on 30/09/2017.
 */

public class DoctorDetailsDataRequest {
    private String photolink;
    private String contact;
    private List<String> specialization;
    private List<String> education;
    private List<String> certification;
    private List<String> award;

    public String getPhotolink() {
        return photolink;
    }

    public void setPhotolink(String photolink) {
        this.photolink = photolink;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public List<String> getSpecialization() {
        return specialization;
    }

    public void setSpecialization(List<String> specialization) {
        this.specialization = specialization;
    }

    public List<String> getEducation() {
        return education;
    }

    public void setEducation(List<String> education) {
        this.education = education;
    }

    public List<String> getCertification() {
        return certification;
    }

    public void setCertification(List<String> certification) {
        this.certification = certification;
    }

    public List<String> getAward() {
        return award;
    }

    public void setAward(List<String> award) {
        this.award = award;
    }
}
