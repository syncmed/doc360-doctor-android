package com.syncmed.doc360_doctor_android.models;

import java.util.List;

/**
 * Created by ernestgayyed on 03/10/2017.
 */

public class NotificationResponse {
    private List<NotificationBody> body;
    private String title;
    private String icon;
    private String sound;

    public List<NotificationBody> getBody() {
        return body;
    }

    public void setBody(List<NotificationBody> body) {
        this.body = body;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getSound() {
        return sound;
    }

    public void setSound(String sound) {
        this.sound = sound;
    }
}
