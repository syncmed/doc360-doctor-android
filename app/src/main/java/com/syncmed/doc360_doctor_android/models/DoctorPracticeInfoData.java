package com.syncmed.doc360_doctor_android.models;

import io.realm.Realm;
import io.realm.RealmObject;

/**
 * Created by ernestgayyed on 29/09/2017.
 */

public class DoctorPracticeInfoData extends RealmObject {
    private String isVoice;
    private String isVideo;
    private String videoRate;
    private String chatRate;
    private ClinicScheduleList schedule;

    public String getIsVoice() {
        return isVoice;
    }

    public void setIsVoice(String isVoice) {
        this.isVoice = isVoice;
    }

    public String getIsVideo() {
        return isVideo;
    }

    public void setIsVideo(String isVideo) {
        this.isVideo = isVideo;
    }

    public String getVideoRate() {
        return videoRate;
    }

    public void setVideoRate(String videoRate) {
        this.videoRate = videoRate;
    }

    public String getChatRate() {
        return chatRate;
    }

    public void setChatRate(String chatRate) {
        this.chatRate = chatRate;
    }

    public ClinicScheduleList getSchedule() {
        return schedule;
    }

    public void setSchedule(ClinicScheduleList schedule) {
        this.schedule = schedule;
    }
}
