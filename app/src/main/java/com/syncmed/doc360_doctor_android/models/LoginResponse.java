package com.syncmed.doc360_doctor_android.models;

/**
 * Created by ernestgayyed on 29/09/2017.
 */

public class LoginResponse {
    private String status;
    private String code;
    private String message;
    private LoginResponseData data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LoginResponseData getData() {
        return data;
    }

    public void setData(LoginResponseData data) {
        this.data = data;
    }
}
