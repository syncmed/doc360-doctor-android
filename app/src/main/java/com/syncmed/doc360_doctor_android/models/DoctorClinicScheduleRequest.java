package com.syncmed.doc360_doctor_android.models;

import java.util.List;

/**
 * Created by ernestgayyed on 29/09/2017.
 */

public class DoctorClinicScheduleRequest {
    private List<ClinicScheduleItem> doctordetails;

    public List<ClinicScheduleItem> getDoctordetails() {
        return doctordetails;
    }

    public void setDoctordetails(List<ClinicScheduleItem> doctordetails) {
        this.doctordetails = doctordetails;
    }
}
