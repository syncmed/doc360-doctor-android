package com.syncmed.doc360_doctor_android.models;

import io.realm.RealmObject;

/**
 * Created by ernestgayyed on 29/09/2017.
 */

public class ClinicScheduleItem extends RealmObject {
    private String id;
    private String location;
    private String day;
    private String time;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }
}
