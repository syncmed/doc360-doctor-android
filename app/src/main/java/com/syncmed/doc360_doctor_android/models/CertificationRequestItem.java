package com.syncmed.doc360_doctor_android.models;

import java.util.List;

/**
 * Created by ernestgayyed on 30/09/2017.
 */

public class CertificationRequestItem {
    private List<String> certification;

    public List<String> getcertification() {
        return certification;
    }

    public void setcertification(List<String> certification) {
        this.certification = certification;
    }
}
