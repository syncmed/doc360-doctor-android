package com.syncmed.doc360_doctor_android.models;

/**
 * Created by ernestgayyed on 01/10/2017.
 */

public class ConsultItem {
    private String id;
    private String consultType;
    private String consultSchedule;
    private Patient patient;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getConsultType() {
        return consultType;
    }

    public void setConsultType(String consultType) {
        this.consultType = consultType;
    }

    public String getConsultSchedule() {
        return consultSchedule;
    }

    public void setConsultSchedule(String consultSchedule) {
        this.consultSchedule = consultSchedule;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }
}
