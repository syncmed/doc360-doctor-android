package com.syncmed.doc360_doctor_android.models;

/**
 * Created by ernestgayyed on 29/09/2017.
 */

public class DoctorInfoResponse {
    private String status;
    private String code;
    private String message;
    private DoctorProfessionalInfoData data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DoctorProfessionalInfoData getData() {
        return data;
    }

    public void setData(DoctorProfessionalInfoData data) {
        this.data = data;
    }
}
