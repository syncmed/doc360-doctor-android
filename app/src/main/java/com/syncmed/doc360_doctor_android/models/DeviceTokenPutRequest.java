package com.syncmed.doc360_doctor_android.models;

import java.util.List;

/**
 * Created by ernestgayyed on 03/10/2017.
 */

public class DeviceTokenPutRequest {
    private List<DeviceToken> details;

    public List<DeviceToken> getDetails() {
        return details;
    }

    public void setDetails(List<DeviceToken> details) {
        this.details = details;
    }
}
