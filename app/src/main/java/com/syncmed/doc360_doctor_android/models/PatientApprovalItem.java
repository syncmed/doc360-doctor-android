package com.syncmed.doc360_doctor_android.models;

/**
 * Created by ernestgayyed on 02/10/2017.
 */

public class PatientApprovalItem {
    private String action;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
