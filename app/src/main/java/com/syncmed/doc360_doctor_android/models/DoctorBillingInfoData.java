package com.syncmed.doc360_doctor_android.models;

import io.realm.RealmObject;

/**
 * Created by ernestgayyed on 29/09/2017.
 */

public class DoctorBillingInfoData extends RealmObject {
    private String bankName;
    private String accountType;
    private String accountNo;

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }
}
