package com.syncmed.doc360_doctor_android.models;

import java.util.List;

/**
 * Created by ernestgayyed on 28/09/2017.
 */

public class SpecializationData {
    private List<SpecializationItem> specialization;

    public List<SpecializationItem> getSpecialization() {
        return specialization;
    }

    public void setSpecialization(List<SpecializationItem> specialization) {
        this.specialization = specialization;
    }
}
