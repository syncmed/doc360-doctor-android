package com.syncmed.doc360_doctor_android.models;

import java.util.List;

/**
 * Created by ernestgayyed on 29/09/2017.
 */

public class DoctorPracticeResponse {
    private String status;
    private String code;
    private String message;
    private List<DoctorPracticeData> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DoctorPracticeData> getData() {
        return data;
    }

    public void setData(List<DoctorPracticeData> data) {
        this.data = data;
    }
}
