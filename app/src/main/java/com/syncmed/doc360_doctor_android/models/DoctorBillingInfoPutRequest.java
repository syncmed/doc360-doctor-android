package com.syncmed.doc360_doctor_android.models;

import java.util.List;

/**
 * Created by ernestgayyed on 29/09/2017.
 */

public class DoctorBillingInfoPutRequest {
    private List<DoctorBillingInfoData> doctordetails;

    public List<DoctorBillingInfoData> getDoctordetails() {
        return doctordetails;
    }

    public void setDoctordetails(List<DoctorBillingInfoData> doctordetails) {
        this.doctordetails = doctordetails;
    }
}
