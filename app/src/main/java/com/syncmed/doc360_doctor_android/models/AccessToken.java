package com.syncmed.doc360_doctor_android.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by ernestgayyed on 14/08/2017.
 */

public class AccessToken extends RealmObject {

    @PrimaryKey
    private long id;
    private String bearerAccessToken;
    private boolean isFirstTimeAccess = true;

    public AccessToken(){
    }

    public String getBearerAccessToken() {
        return bearerAccessToken;
    }

    public void setBearerAccessToken(String bearerAccessToken) {
        this.bearerAccessToken = "Bearer " + bearerAccessToken;
        //this.bearerAccessToken = bearerAccessToken;
    }

    public boolean isFirstTimeAccess() {
        return isFirstTimeAccess;
    }

    public void setFirstTimeAccess(boolean firstTimeAccess) {
        isFirstTimeAccess = firstTimeAccess;
    }
}
