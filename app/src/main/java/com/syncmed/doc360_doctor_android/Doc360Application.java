package com.syncmed.doc360_doctor_android;

import android.app.Application;
import android.content.Context;

import com.orm.SugarApp;
import com.syncmed.doc360_doctor_android.components.AppComponent;
import com.syncmed.doc360_doctor_android.components.DaggerAppComponent;
import com.syncmed.doc360_doctor_android.components.DaggerRequestComponent;
import com.syncmed.doc360_doctor_android.components.RequestComponent;
import com.syncmed.doc360_doctor_android.modules.AppModule;
import com.syncmed.doc360_doctor_android.modules.RequestModule;

import io.realm.Realm;

/**
 * Created by ernestgayyed on 10/08/2017.
 */

public class Doc360Application extends Application {
    AppComponent appComponent;
    RequestComponent requestComponent;

    public static Doc360Application get(Context context) {
        return (Doc360Application) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();

        requestComponent = DaggerRequestComponent.builder()
                .appModule(new AppModule(this))
                .requestModule(new RequestModule())
                .build();

        Realm.init(this);
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    public RequestComponent getRequestComponent() {
        return requestComponent;
    }
}
