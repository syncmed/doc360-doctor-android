package com.syncmed.doc360_doctor_android.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.syncmed.doc360_doctor_android.Doc360Application;
import com.syncmed.doc360_doctor_android.R;
import com.syncmed.doc360_doctor_android.activities.HomeActivity;
import com.syncmed.doc360_doctor_android.adapters.PatientRequestListAdapter;
import com.syncmed.doc360_doctor_android.components.DaggerPatientRequestComponent;
import com.syncmed.doc360_doctor_android.interfaces.PatientContract;
import com.syncmed.doc360_doctor_android.models.CareTeamItem;
import com.syncmed.doc360_doctor_android.modules.PatientRequestModule;
import com.syncmed.doc360_doctor_android.presenters.PatientRequestPresenter;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * A simple {@link Fragment} subclass.
 */
public class PatientRequestFragment extends Fragment implements PatientContract.RequestView, PatientContract.PatientRequestListener {


    @BindView(R.id.patient_request_list)
    RecyclerView patientRequestList;
    @BindView(R.id.back_button)
    ImageView backButton;
    Unbinder unbinder;

    @Inject
    PatientRequestPresenter patientRequestPresenter;

    private PatientRequestListAdapter patientRequestListAdapter;
    private SweetAlertDialog pDialog;

    public PatientRequestFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        DaggerPatientRequestComponent.builder()
                .requestComponent(((Doc360Application) getActivity().getApplicationContext()).getRequestComponent())
                .patientRequestModule(new PatientRequestModule(this))
                .build().inject(this);

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_patient_request, container, false);
        unbinder = ButterKnife.bind(this, view);

        patientRequestPresenter.onViewCreated();

        return view;
    }

    @Override
    public void showLoadingView() {
        pDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
        pDialog.setTitleText("Saving...");
        pDialog.setCancelable(false);
        pDialog.show();
    }

    @Override
    public void hideLoadingView() {
        pDialog.dismiss();
    }

    @Override
    public void showErrorMessage(String title, String message) {

    }

    @Override
    public void showSuccessMessage(String title, String message) {

    }

    @Override
    public void setupListAdapter() {
        patientRequestListAdapter = new PatientRequestListAdapter();
        patientRequestListAdapter.setPatientRequestListener(this);
        patientRequestList.setLayoutManager(new LinearLayoutManager(getContext()));
        patientRequestList.setAdapter(patientRequestListAdapter);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().popBackStackImmediate();
            }
        });
    }

    @Override
    public void extractBundleData() {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void loadPatientRequestList(List<CareTeamItem> patientRequestList) {
        patientRequestListAdapter.addList(patientRequestList);
    }

    @Override
    public void onPatientAccepted(int position) {
        CareTeamItem careTeamItem = patientRequestListAdapter.getList().get(position);
        patientRequestPresenter.postPatientRequest(careTeamItem.getRecordID(), "1");
    }

    @Override
    public void onPatientDeclined(int position) {
        CareTeamItem careTeamItem = patientRequestListAdapter.getList().get(position);
        patientRequestPresenter.postPatientRequest(careTeamItem.getRecordID(), "0");
    }

    @Override
    public void setPatientRequestSummary(List<CareTeamItem> patientRequestSummary) {
        ((HomeActivity) getActivity()).setPatientRequest(patientRequestSummary);
    }
}
