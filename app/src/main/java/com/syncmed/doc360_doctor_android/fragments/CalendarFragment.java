package com.syncmed.doc360_doctor_android.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.syncmed.doc360_doctor_android.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class CalendarFragment extends Fragment {


    public CalendarFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_calendar, container, false);
    }

    public static CalendarFragment newInstance(String message) {
        CalendarFragment fragment = new CalendarFragment();

        Bundle args = new Bundle();
        args.putString("calendar", message);
        fragment.setArguments(args);

        return fragment;
    }

}
