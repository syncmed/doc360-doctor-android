package com.syncmed.doc360_doctor_android.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.syncmed.doc360_doctor_android.Doc360Application;
import com.syncmed.doc360_doctor_android.R;
import com.syncmed.doc360_doctor_android.activities.HomeActivity;
import com.syncmed.doc360_doctor_android.adapters.PatientListAdapter;
import com.syncmed.doc360_doctor_android.adapters.PatientRequestListAdapter;
import com.syncmed.doc360_doctor_android.adapters.PatientViewPagerAdapter;
import com.syncmed.doc360_doctor_android.components.DaggerPatientComponent;
import com.syncmed.doc360_doctor_android.helpers.Utility;
import com.syncmed.doc360_doctor_android.interfaces.PatientContract;
import com.syncmed.doc360_doctor_android.models.CareTeamItem;
import com.syncmed.doc360_doctor_android.modules.PatientModule;
import com.syncmed.doc360_doctor_android.presenters.PatientPresenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * A simple {@link Fragment} subclass.
 */
public class PatientFragment extends Fragment implements PatientContract.View {


    @Inject
    PatientPresenter patientPresenter;
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.textView)
    TextView textView;
    @BindView(R.id.header_layout)
    FrameLayout headerLayout;
    @BindView(R.id.search_patient_text)
    EditText searchPatientText;
    @BindView(R.id.patient_list)
    RecyclerView patientList;
    @BindView(R.id.patient_list_swipe)
    SwipeRefreshLayout patientListSwipe;
    @BindView(R.id.no_patient_label)
    TextView noPatientLabel;
    Unbinder unbinder;

    private SweetAlertDialog pDialog;
    private PatientViewPagerAdapter patientViewPagerAdapter;
    private PatientRequestListAdapter patientRequestListAdapter;
    private PatientListAdapter patientListAdapter;
    private List<CareTeamItem> tempPatientList;

    public PatientFragment() {
        // Required empty public constructor
    }

    public static PatientFragment newInstance(String message) {
        PatientFragment fragment = new PatientFragment();

        Bundle args = new Bundle();
        args.putString("message", message);
        fragment.setArguments(args);

        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_patient, container, false);
        unbinder = ButterKnife.bind(this, view);

        DaggerPatientComponent.builder()
                .requestComponent(((Doc360Application) getActivity().getApplicationContext()).getRequestComponent())
                .patientModule(new PatientModule(this))
                .build().inject(this);

        patientPresenter.onViewCreated();

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void showLoadingView() {
        pDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
        pDialog.setTitleText("Saving...");
        pDialog.setCancelable(false);
        pDialog.show();
    }

    @Override
    public void hideLoadingView() {
        pDialog.dismiss();
    }

    @Override
    public void showErrorMessage(String title, String message) {

    }

    @Override
    public void showSuccessMessage(String title, String message) {
        new SweetAlertDialog(getContext())
                .setTitleText(title)
                .setContentText(message)
                .show();
    }

    @Override
    public void setupListAdapter() {
        tempPatientList = new ArrayList<>();

        patientListAdapter = new PatientListAdapter();
        patientList.setLayoutManager(new LinearLayoutManager(getContext()));
        patientList.setAdapter(patientListAdapter);

        patientListSwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                patientPresenter.loadPatientList();
            }
        });

        searchPatientText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (searchPatientText.hasFocus()) {
                    if (!Utility.isStringEmptyOrNull(editable.toString())) {
                        if (tempPatientList.size() == 0) {
                            tempPatientList = patientListAdapter.getList();
                        }

                        List<CareTeamItem> patientList = tempPatientList;
                        List<CareTeamItem> result = new ArrayList<CareTeamItem>();
                        for (CareTeamItem patient : patientList) {
                            String name = patient.getFname() + " " + patient.getLname();
                            if (name.toLowerCase().contains(editable.toString().toLowerCase())) {
                                result.add(patient);
                            }
                        }

                        loadPatientList(result);
                    } else {
                        loadPatientList(tempPatientList);
                        //tempPatientList.clear();
                    }
                }
            }
        });

    }

    @Override
    public void extractBundleData() {

    }

    @Override
    public void loadPatientList(List<CareTeamItem> list) {
        patientListSwipe.setRefreshing(false);
        if (list.size() > 0) {
            noPatientLabel.setVisibility(View.GONE);
            patientList.setVisibility(View.VISIBLE);
            patientListAdapter.addList(list);
        } else {
            noPatientLabel.setVisibility(View.VISIBLE);
            patientList.setVisibility(View.GONE);
        }
    }

    @Override
    public void reloadPatientRequestBadge(String count) {
        ((HomeActivity) getActivity()).setPatientRequestBadge(count);
    }
}