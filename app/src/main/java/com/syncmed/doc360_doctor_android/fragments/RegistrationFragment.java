package com.syncmed.doc360_doctor_android.fragments;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TextView;

import com.github.ybq.android.spinkit.SpinKitView;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;
import com.meg7.widget.CircleImageView;
import com.syncmed.doc360_doctor_android.Doc360Application;
import com.syncmed.doc360_doctor_android.R;
import com.syncmed.doc360_doctor_android.activities.HomeActivity;
import com.syncmed.doc360_doctor_android.adapters.ClinicScheduleListAdapter;
import com.syncmed.doc360_doctor_android.adapters.RegistrationPagerAdapter;
import com.syncmed.doc360_doctor_android.adapters.SearchListAdapter;
import com.syncmed.doc360_doctor_android.adapters.SpecializationListAdapter;
import com.syncmed.doc360_doctor_android.adapters.StringListAdapter;
import com.syncmed.doc360_doctor_android.components.DaggerRegistrationComponent;
import com.syncmed.doc360_doctor_android.helpers.GlideImageUtil;
import com.syncmed.doc360_doctor_android.helpers.Utility;
import com.syncmed.doc360_doctor_android.interfaces.RegistrationContract;
import com.syncmed.doc360_doctor_android.models.ClinicScheduleItem;
import com.syncmed.doc360_doctor_android.models.ClinicScheduleList;
import com.syncmed.doc360_doctor_android.models.DoctorBillingInfoData;
import com.syncmed.doc360_doctor_android.models.DoctorPracticeInfoData;
import com.syncmed.doc360_doctor_android.models.DoctorProfessionalInfoData;
import com.syncmed.doc360_doctor_android.models.DoctorProfileData;
import com.syncmed.doc360_doctor_android.models.Specialization;
import com.syncmed.doc360_doctor_android.models.SpecializationItem;
import com.syncmed.doc360_doctor_android.modules.RegistrationModule;
import com.syncmed.doc360_doctor_android.presenters.RegistrationPresenter;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;
import me.iwf.photopicker.PhotoPicker;
import me.relex.circleindicator.CircleIndicator;

import static android.app.Activity.RESULT_OK;


/**
 * A simple {@link Fragment} subclass.
 */
public class RegistrationFragment extends Fragment implements RegistrationContract.View, RegistrationContract.DoctorScheduleListener, RegistrationContract.BillingInfoListener, RegistrationContract.DoctorRateListener, RegistrationContract.ItemListener {


    @BindView(R.id.profile_image_button)
    CircleImageView profileImageButton;
    @BindView(R.id.name_text)
    TextView nameText;
    @BindView(R.id.specialization_text)
    EditText specializationText;
    @BindView(R.id.education_text)
    EditText educationText;
    @BindView(R.id.education_list)
    RecyclerView educationList;
    @BindView(R.id.cert_memberships_text)
    EditText certMembershipsText;
    @BindView(R.id.cert_memberships_list)
    RecyclerView certMembershipsList;
    @BindView(R.id.awards_publications_text)
    EditText awardsPublicationsText;
    @BindView(R.id.awards_publications_list)
    RecyclerView awardsPublicationsList;
    @BindView(R.id.specialization_search_list)
    RecyclerView specializationSearchList;
    @BindView(R.id.page_one)
    ScrollView pageOne;
    @BindView(R.id.view_pager)
    ViewPager viewPager;
    @BindView(R.id.indicator)
    CircleIndicator indicator;
    @BindView(R.id.add_schedule_button)
    ImageButton addScheduleButton;
    @BindView(R.id.monday_sched_list)
    RecyclerView mondaySchedList;
    @BindView(R.id.tuesday_sched_list)
    RecyclerView tuesdaySchedList;
    @BindView(R.id.wednesday_sched_list)
    RecyclerView wednesdaySchedList;
    @BindView(R.id.thursday_sched_list)
    RecyclerView thursdaySchedList;
    @BindView(R.id.friday_sched_list)
    RecyclerView fridaySchedList;
    @BindView(R.id.page_two)
    ScrollView pageTwo;
    @BindView(R.id.virtual_consults_label)
    TextView virtualConsultsLabel;
    @BindView(R.id.video_consult_switch)
    Switch videoConsultSwitch;
    @BindView(R.id.voice_consult_switch)
    Switch voiceConsultSwitch;
    @BindView(R.id.consults_rate_label)
    TextView consultsRateLabel;
    @BindView(R.id.edit_consult_charge_button)
    ImageButton editConsultChargeButton;
    @BindView(R.id.voice_video_charge_text)
    TextView voiceVideoChargeText;
    @BindView(R.id.chat_charge_text)
    TextView chatChargeText;
    @BindView(R.id.page_three)
    ScrollView pageThree;
    @BindView(R.id.edit_bank_button)
    ImageButton editBankButton;
    @BindView(R.id.bank_name_text)
    TextView bankNameText;
    @BindView(R.id.acct_type_text)
    TextView acctTypeText;
    @BindView(R.id.acct_no_text)
    TextView acctNoText;
    @BindView(R.id.page_four)
    ScrollView pageFour;
    @BindView(R.id.specialization_list)
    RecyclerView specializationList;
    @BindView(R.id.contact_text)
    EditText contactText;
    @BindView(R.id.save_button)
    Button saveButton;
    @BindView(R.id.spin_kit)
    SpinKitView spinKit;
    @BindView(R.id.back_button)
    ImageButton backButton;
    Unbinder unbinder;

    @Inject
    RegistrationPresenter presenter;

    private SweetAlertDialog pDialog;
    private FirebaseStorage firebaseStorage;
    private StorageReference storageRef;
    private Timer timer = new Timer();
    private final long DELAY = 500;
    private List<Uri> mSelected;
    private StringListAdapter educationListAdapter;
    private StringListAdapter certMembershipsListAdapter;
    private StringListAdapter awardsPublicationsListAdapter;
    private SearchListAdapter specializationSearchAdapter;
    private SpecializationListAdapter specializationListAdapter;

    private ClinicScheduleListAdapter mondaySchedAdapter;
    private ClinicScheduleListAdapter tuesdaySchedAdapter;
    private ClinicScheduleListAdapter wednesdaySchedAdapter;
    private ClinicScheduleListAdapter thursdaySchedAdapter;
    private ClinicScheduleListAdapter fridaySchedAdapter;

    private DoctorProfileData doctorProfileData;

    public RegistrationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        DaggerRegistrationComponent.builder()
                .requestComponent(((Doc360Application) getActivity().getApplicationContext()).getRequestComponent())
                .registrationModule(new RegistrationModule(this))
                .build().inject(this);

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_registration, container, false);
        unbinder = ButterKnife.bind(this, view);

        firebaseStorage = FirebaseStorage.getInstance();
        storageRef = firebaseStorage.getReference();

        presenter.onViewCreated();

        return view;
    }

    private void showImagePickerDialog() {
        if (isStoragePermissionGranted()) {
            PhotoPicker.builder()
                    .setPhotoCount(1)
                    .setShowCamera(true)
                    .setShowGif(true)
                    .setPreviewEnabled(false)
                    .start(getActivity(), PhotoPicker.REQUEST_CODE);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == 100 && resultCode == RESULT_OK) {
//            mSelected = Matisse.obtainResult(data);
//            Log.d("Matisse", "mSelected: " + mSelected);
//
//            profileImageButton.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.img_default_profile));
//            spinKit.setVisibility(View.VISIBLE);
//
//            uploadProfileImageToStorage(mSelected.get(0));
//        }

        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == PhotoPicker.REQUEST_CODE) {
            if (data != null) {
                ArrayList<String> photos = data.getStringArrayListExtra(PhotoPicker.KEY_SELECTED_PHOTOS);

                profileImageButton.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.img_default_profile));
                spinKit.setVisibility(View.VISIBLE);

                uploadProfileImageToStorage(Uri.fromFile(new File(photos.get(0))));
            }
        }
    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2000);
                return false;
            }
        } else {
            return true;
        }
    }

    public void uploadProfileImageToStorage(Uri uri) {
        StorageReference imagesRef = storageRef.child("profile_images");
        StorageReference accountImageRef = imagesRef.child("doctors");

        String name = doctorProfileData.getProfessionalInfo().getFname() + "_" + doctorProfileData.getProfessionalInfo().getLname() + "_" + String.valueOf(new Date().getTime());
        StorageReference uniqueImageRef = accountImageRef.child(name);

        UploadTask uploadTask = uniqueImageRef.putFile(uri);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
                spinKit.setVisibility(View.GONE);
                Log.d("error", "error");
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                spinKit.setVisibility(View.GONE);
                Uri downloadUrl = taskSnapshot.getDownloadUrl();
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                GlideImageUtil glideImageUtil = new GlideImageUtil(getContext());
                glideImageUtil.setCircularImage(downloadUrl, R.drawable.img_default_profile, profileImageButton);
                doctorProfileData.getProfessionalInfo().setPhotolink(downloadUrl.toString());
            }
        });
    }

    @Override
    public void presentDoctorProfessionalInfo(DoctorProfessionalInfoData data) {
        nameText.setText(data.getFname() + " " + data.getLname());
        contactText.setText(data.getContact());
        specializationListAdapter.addList(data.getSpecializationList());
        educationListAdapter.addList(data.getProfessionalInfo().getEducation());
        certMembershipsListAdapter.addList(data.getProfessionalInfo().getCertification());
        awardsPublicationsListAdapter.addList(data.getProfessionalInfo().getAward());

        GlideImageUtil glideImageUtil = new GlideImageUtil(getContext());
        glideImageUtil.setCircularImage(data.getPhotolink(), R.drawable.img_default_profile, profileImageButton);
    }

    @Override
    public void presentDoctorBillingInfo(DoctorBillingInfoData data) {
        bankNameText.setText(data.getBankName());
        acctTypeText.setText(data.getAccountType());
        acctNoText.setText(data.getAccountNo());
    }

    @Override
    public void presentDoctorPracticeInfo(DoctorPracticeInfoData data) {
        voiceVideoChargeText.setText(data.getVideoRate());
        chatChargeText.setText(data.getChatRate());

        if (data.getIsVideo().equals("1")) {
            videoConsultSwitch.setChecked(true);
        } else {
            videoConsultSwitch.setChecked(false);
        }

        if (data.getIsVoice().equals("1")) {
            voiceConsultSwitch.setChecked(true);
        } else {
            voiceConsultSwitch.setChecked(false);
        }

        loadClinicSchedule(data.getSchedule());
    }

    @Override
    public void showLoadingView() {
        pDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
        pDialog.setTitleText("Saving...");
        pDialog.setCancelable(false);
        pDialog.show();
    }

    @Override
    public void hideLoadingView() {
        pDialog.dismiss();
    }

    @Override
    public void showErrorMessage(String title, String message) {

    }

    @Override
    public void showSuccessMessage(String title, String message) {
        new SweetAlertDialog(getContext())
                .setTitleText(title)
                .setContentText(message)
                .show();
    }

    @Override
    public void setupListAdapter() {
        RegistrationPagerAdapter adapter = new RegistrationPagerAdapter(getContext());
        viewPager.setOffscreenPageLimit(4);
        viewPager.setAdapter(adapter);
        indicator.setViewPager(viewPager);

        specializationListAdapter = new SpecializationListAdapter();
        specializationListAdapter.setItemListener(new RegistrationContract.ItemListener() {
            @Override
            public void onItemClicked(int position) {

            }

            @Override
            public void onItemEdited(int position) {

            }

            @Override
            public void onItemDeleted(int position) {
                specializationListAdapter.removeItem(position);
            }
        });
        specializationList.setLayoutManager(new LinearLayoutManager(getContext()));
        specializationList.setAdapter(specializationListAdapter);

        educationListAdapter = new StringListAdapter();
        educationList.setLayoutManager(new LinearLayoutManager(getContext()));
        educationList.setAdapter(educationListAdapter);

        certMembershipsListAdapter = new StringListAdapter();
        certMembershipsList.setLayoutManager(new LinearLayoutManager(getContext()));
        certMembershipsList.setAdapter(certMembershipsListAdapter);

        awardsPublicationsListAdapter = new StringListAdapter();
        awardsPublicationsList.setLayoutManager(new LinearLayoutManager(getContext()));
        awardsPublicationsList.setAdapter(awardsPublicationsListAdapter);

        specializationSearchAdapter = new SearchListAdapter();
        specializationSearchAdapter.setItemListener(this);
        specializationSearchList.setLayoutManager(new LinearLayoutManager(getContext()));
        specializationSearchList.setAdapter(specializationSearchAdapter);

        mondaySchedAdapter = new ClinicScheduleListAdapter();
        mondaySchedAdapter.setItemListener(new RegistrationContract.ItemListener() {
            @Override
            public void onItemClicked(int position) {

            }

            @Override
            public void onItemEdited(int position) {
                ClinicScheduleItem clinicScheduleItem = mondaySchedAdapter.getList().get(position);
                clinicScheduleItem.setDay("Monday");
                showEditSchedule(clinicScheduleItem);
            }

            @Override
            public void onItemDeleted(int position) {
                presenter.deleteDoctorSchedule(mondaySchedAdapter.getList().get(position).getId());
            }
        });
        mondaySchedList.setLayoutManager(new LinearLayoutManager(getContext()));
        mondaySchedList.setAdapter(mondaySchedAdapter);

        tuesdaySchedAdapter = new ClinicScheduleListAdapter();
        tuesdaySchedAdapter.setItemListener(new RegistrationContract.ItemListener() {
            @Override
            public void onItemClicked(int position) {

            }

            @Override
            public void onItemEdited(int position) {
                ClinicScheduleItem clinicScheduleItem = tuesdaySchedAdapter.getList().get(position);
                clinicScheduleItem.setDay("Tuesday");
                showEditSchedule(clinicScheduleItem);
            }

            @Override
            public void onItemDeleted(int position) {
                presenter.deleteDoctorSchedule(tuesdaySchedAdapter.getList().get(position).getId());
            }
        });
        tuesdaySchedList.setLayoutManager(new LinearLayoutManager(getContext()));
        tuesdaySchedList.setAdapter(tuesdaySchedAdapter);

        wednesdaySchedAdapter = new ClinicScheduleListAdapter();
        wednesdaySchedAdapter.setItemListener(new RegistrationContract.ItemListener() {
            @Override
            public void onItemClicked(int position) {

            }

            @Override
            public void onItemEdited(int position) {
                ClinicScheduleItem clinicScheduleItem = wednesdaySchedAdapter.getList().get(position);
                clinicScheduleItem.setDay("Wednesday");
                showEditSchedule(clinicScheduleItem);
            }

            @Override
            public void onItemDeleted(int position) {
                presenter.deleteDoctorSchedule(wednesdaySchedAdapter.getList().get(position).getId());
            }
        });
        wednesdaySchedList.setLayoutManager(new LinearLayoutManager(getContext()));
        wednesdaySchedList.setAdapter(wednesdaySchedAdapter);

        thursdaySchedAdapter = new ClinicScheduleListAdapter();
        thursdaySchedAdapter.setItemListener(new RegistrationContract.ItemListener() {
            @Override
            public void onItemClicked(int position) {

            }

            @Override
            public void onItemEdited(int position) {
                ClinicScheduleItem clinicScheduleItem = thursdaySchedAdapter.getList().get(position);
                clinicScheduleItem.setDay("Thursday");
                showEditSchedule(clinicScheduleItem);
            }

            @Override
            public void onItemDeleted(int position) {
                presenter.deleteDoctorSchedule(thursdaySchedAdapter.getList().get(position).getId());
            }
        });
        thursdaySchedList.setLayoutManager(new LinearLayoutManager(getContext()));
        thursdaySchedList.setAdapter(thursdaySchedAdapter);

        fridaySchedAdapter = new ClinicScheduleListAdapter();
        fridaySchedAdapter.setItemListener(new RegistrationContract.ItemListener() {
            @Override
            public void onItemClicked(int position) {

            }

            @Override
            public void onItemEdited(int position) {
                ClinicScheduleItem clinicScheduleItem = fridaySchedAdapter.getList().get(position);
                clinicScheduleItem.setDay("Friday");
                showEditSchedule(clinicScheduleItem);
            }

            @Override
            public void onItemDeleted(int position) {
                presenter.deleteDoctorSchedule(fridaySchedAdapter.getList().get(position).getId());
            }
        });
        fridaySchedList.setLayoutManager(new LinearLayoutManager(getContext()));
        fridaySchedList.setAdapter(fridaySchedAdapter);

        specializationText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (specializationText.getRight() - specializationText.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        if (!Utility.isStringEmptyOrNull(specializationText.getText().toString())) {
                            Specialization specialization = new Specialization();
                            specialization.setSpecializationID(specializationText.getTag().toString());
                            specialization.setSpecialization(specializationText.getText().toString());

                            specializationListAdapter.addItem(specialization);
                            specializationText.setText("");
                        }
                        return true;
                    }
                }
                return false;
            }
        });

        educationText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (educationText.getRight() - educationText.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        if (!Utility.isStringEmptyOrNull(educationText.getText().toString())) {
                            educationListAdapter.addItem(educationText.getText().toString());
                            educationText.setText("");
                        }
                        return true;
                    }
                }
                return false;
            }
        });

        awardsPublicationsText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (awardsPublicationsText.getRight() - awardsPublicationsText.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        if (!Utility.isStringEmptyOrNull(awardsPublicationsText.getText().toString())) {
                            awardsPublicationsListAdapter.addItem(awardsPublicationsText.getText().toString());
                            awardsPublicationsText.setText("");
                        }
                        return true;
                    }
                }
                return false;
            }
        });

        certMembershipsText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (certMembershipsText.getRight() - certMembershipsText.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        if (!Utility.isStringEmptyOrNull(certMembershipsText.getText().toString())) {
                            certMembershipsListAdapter.addItem(certMembershipsText.getText().toString());
                            certMembershipsText.setText("");
                        }
                        return true;
                    }
                }
                return false;
            }
        });

        specializationText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(final Editable editable) {
                timer.cancel();
                timer = new Timer();
                timer.schedule(
                        new TimerTask() {
                            @Override
                            public void run() {
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (specializationText.hasFocus()) {
                                            presenter.searchSpecialization(editable.toString());
                                        }
                                    }
                                });
                            }
                        },
                        DELAY
                );
            }
        });

        contactText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                doctorProfileData.getProfessionalInfo().setContact(editable.toString());
            }
        });

        voiceConsultSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    doctorProfileData.getPracticeInfo().setIsVoice("1");
                } else {
                    doctorProfileData.getPracticeInfo().setIsVoice("0");
                }
            }
        });

        videoConsultSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    doctorProfileData.getPracticeInfo().setIsVideo("1");
                } else {
                    doctorProfileData.getPracticeInfo().setIsVideo("0");
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.back_button, R.id.add_schedule_button, R.id.profile_image_button, R.id.edit_consult_charge_button, R.id.edit_bank_button, R.id.save_button})
    public void onViewClicked(View view) {
        Bundle bundle = new Bundle();
        Gson gson = new Gson();
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        switch (view.getId()) {
            case R.id.back_button:
                getActivity().getSupportFragmentManager().popBackStackImmediate();
                break;
            case R.id.add_schedule_button:
                bundle.putString("action", "clinic_schedules");

                RegistrationDialogFragment addDialogFragment = new RegistrationDialogFragment();
                addDialogFragment.setTargetFragment(this, R.string.schedule_request_code);
                addDialogFragment.setArguments(bundle);
                addDialogFragment.show(fragmentTransaction, "RegistrationDialogFragment");
                break;
            case R.id.profile_image_button:
                showImagePickerDialog();
                break;
            case R.id.edit_consult_charge_button:
                bundle.putString("action", "consult_charges");
                bundle.putString("chat_rate", new Gson().toJson(doctorProfileData.getPracticeInfo().getChatRate()));
                bundle.putString("video_voice_rate", new Gson().toJson(doctorProfileData.getPracticeInfo().getVideoRate()));

                RegistrationDialogFragment editConsultFragment = new RegistrationDialogFragment();
                editConsultFragment.setTargetFragment(this, R.string.rate_request_code);
                editConsultFragment.setArguments(bundle);
                editConsultFragment.show(fragmentTransaction, "RegistrationDialogFragment");
                break;
            case R.id.edit_bank_button:
                bundle.putString("action", "bank_details");
                bundle.putString("billing_info", new Gson().toJson(doctorProfileData.getBillInfo()));

                RegistrationDialogFragment editBankFragment = new RegistrationDialogFragment();
                editBankFragment.setTargetFragment(this, R.string.bank_request_code);
                editBankFragment.setArguments(bundle);
                editBankFragment.show(fragmentTransaction, "RegistrationDialogFragment");
                break;
            case R.id.save_button:
                presenter.onSaveClicked(doctorProfileData);
                break;
        }
    }

    @Override
    public void loadspecializationSearchList(List<SpecializationItem> specializationItemList) {

    }

    @Override
    public void loadSpecializationList(List<SpecializationItem> specializationItemList) {
        specializationSearchAdapter.addList(specializationItemList);
    }

    @Override
    public void loadDoctorProfileData(DoctorProfileData doctorProfileData) {
        this.doctorProfileData = doctorProfileData;
    }

    @Override
    public void onItemClicked(int position) {
        SpecializationItem specializationItem = specializationSearchAdapter.getList().get(position);
        specializationText.setText(specializationItem.getDesc());
        specializationText.setTag(specializationItem.getId());

        specializationSearchAdapter.removeSearchList();
        specializationText.setFocusableInTouchMode(false);
        specializationText.setFocusable(false);
        specializationText.setFocusableInTouchMode(true);
        specializationText.setFocusable(true);
    }

    @Override
    public void onItemEdited(int position) {

    }

    @Override
    public void onItemDeleted(int position) {

    }

    @Override
    public void clearSearchList() {
        specializationSearchAdapter.removeSearchList();
    }

    @Override
    public void loadClinicSchedule(ClinicScheduleList scheduleList) {
        if (scheduleList.getMonday() != null) {
            mondaySchedAdapter.addList(scheduleList.getMonday());
        }

        if (scheduleList.getTuesday() != null) {
            tuesdaySchedAdapter.addList(scheduleList.getTuesday());
        }

        if (scheduleList.getWednesday() != null) {
            wednesdaySchedAdapter.addList(scheduleList.getWednesday());
        }

        if (scheduleList.getThursday() != null) {
            thursdaySchedAdapter.addList(scheduleList.getThursday());
        }

        if (scheduleList.getFriday() != null) {
            fridaySchedAdapter.addList(scheduleList.getFriday());
        }
    }

    @Override
    public void extractBundleData() {

    }

    @Override
    public void onBillingDataSaved(DoctorBillingInfoData data) {
        doctorProfileData.setBillInfo(data);
        presentDoctorBillingInfo(data);
    }

    @Override
    public void onRateSaved(String voiceVideoRate, String chatRate) {
        doctorProfileData.getPracticeInfo().setChatRate(chatRate);
        doctorProfileData.getPracticeInfo().setVideoRate(voiceVideoRate);

        presentDoctorPracticeInfo(doctorProfileData.getPracticeInfo());
    }

    @Override
    public void onScheduleSaved(ClinicScheduleItem scheduleItem) {
        presenter.getDoctorPractice();
    }

    public void showEditSchedule(ClinicScheduleItem clinicScheduleItem) {
        Bundle bundle = new Bundle();
        Gson gson = new Gson();
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        bundle.putString("action", "clinic_schedules");
        bundle.putString("clinic_schedules", gson.toJson(clinicScheduleItem));

        RegistrationDialogFragment addDialogFragment = new RegistrationDialogFragment();
        addDialogFragment.setTargetFragment(this, R.string.schedule_request_code);
        addDialogFragment.setArguments(bundle);
        addDialogFragment.show(fragmentTransaction, "RegistrationDialogFragment");
    }

    @Override
    public void presentHomeView() {
        Intent intent = new Intent(getContext(), HomeActivity.class);
        getActivity().startActivity(intent);
        getActivity().finish();
    }
}