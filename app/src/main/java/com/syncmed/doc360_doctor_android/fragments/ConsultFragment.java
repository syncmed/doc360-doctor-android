package com.syncmed.doc360_doctor_android.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.syncmed.doc360_doctor_android.Doc360Application;
import com.syncmed.doc360_doctor_android.R;
import com.syncmed.doc360_doctor_android.activities.HomeActivity;
import com.syncmed.doc360_doctor_android.adapters.ConsultRequestListAdapter;
import com.syncmed.doc360_doctor_android.components.DaggerConsultComponent;
import com.syncmed.doc360_doctor_android.interfaces.ConsultContract;
import com.syncmed.doc360_doctor_android.models.ConsultRequest;
import com.syncmed.doc360_doctor_android.modules.ConsultModule;
import com.syncmed.doc360_doctor_android.presenters.ConsultPresenter;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConsultFragment extends Fragment implements ConsultContract.View {


    @BindView(R.id.header_layout)
    FrameLayout headerLayout;
    @BindView(R.id.no_request_label)
    TextView noRequestLabel;
    @BindView(R.id.consult_request_list)
    RecyclerView consultRequestList;
    @BindView(R.id.consult_request_swipe)
    SwipeRefreshLayout consultRequestSwipe;
    @BindView(R.id.back_button)
    ImageView backButton;
    Unbinder unbinder;

    @Inject
    ConsultPresenter presenter;


    private ConsultRequestListAdapter consultRequestListAdapter;

    public ConsultFragment() {
        // Required empty public constructor
    }

    public static ConsultFragment newInstance(String message) {
        ConsultFragment fragment = new ConsultFragment();

        Bundle args = new Bundle();
        args.putString("message", message);
        fragment.setArguments(args);

        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        DaggerConsultComponent.builder()
                .requestComponent(((Doc360Application) getActivity().getApplicationContext()).getRequestComponent())
                .consultModule(new ConsultModule(this))
                .build().inject(this);
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_consult, container, false);
        unbinder = ButterKnife.bind(this, view);

        presenter.onViewCreated();

        return view;
    }

    @Override
    public void showLoadingView() {

    }

    @Override
    public void hideLoadingView() {

    }

    @Override
    public void showErrorMessage(String title, String message) {

    }

    @Override
    public void showSuccessMessage(String title, String message) {

    }

    @Override
    public void setupListAdapter() {
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().popBackStackImmediate();
            }
        });

        consultRequestListAdapter = new ConsultRequestListAdapter();
        consultRequestList.setLayoutManager(new LinearLayoutManager(getContext()));
        consultRequestList.setAdapter(consultRequestListAdapter);

        consultRequestSwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.loadConsultRequests();
            }
        });
    }

    @Override
    public void loadConsultRequestList(List<ConsultRequest> list) {
        consultRequestSwipe.setRefreshing(false);

        if(list.size() > 0) {
            noRequestLabel.setVisibility(View.GONE);
            consultRequestList.setVisibility(View.VISIBLE);
            consultRequestListAdapter.addList(list);
        } else {
            noRequestLabel.setVisibility(View.VISIBLE);
            consultRequestList.setVisibility(View.GONE);
        }
    }

    @Override
    public void extractBundleData() {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void reloadConsultRequestBadge(String count) {
        ((HomeActivity) getActivity()).setConsultRequestBadge(count);
    }
}
