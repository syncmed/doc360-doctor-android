package com.syncmed.doc360_doctor_android.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.meg7.widget.CircleImageView;
import com.syncmed.doc360_doctor_android.Doc360Application;
import com.syncmed.doc360_doctor_android.R;
import com.syncmed.doc360_doctor_android.activities.HomeActivity;
import com.syncmed.doc360_doctor_android.adapters.ConsultListAdapter;
import com.syncmed.doc360_doctor_android.adapters.ConsultScheduleListAdapter;
import com.syncmed.doc360_doctor_android.adapters.PatientListAdapter;
import com.syncmed.doc360_doctor_android.components.DaggerLandingComponent;
import com.syncmed.doc360_doctor_android.helpers.GlideImageUtil;
import com.syncmed.doc360_doctor_android.interfaces.LandingContract;
import com.syncmed.doc360_doctor_android.models.CareTeamItem;
import com.syncmed.doc360_doctor_android.models.ConsultItem;
import com.syncmed.doc360_doctor_android.models.ConsultRequest;
import com.syncmed.doc360_doctor_android.models.DoctorProfessionalInfoData;
import com.syncmed.doc360_doctor_android.modules.LandingModule;
import com.syncmed.doc360_doctor_android.presenters.LandingPresenter;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class LandingFragment extends Fragment implements LandingContract.View {


    @BindView(R.id.user_image)
    CircleImageView userImage;
    @BindView(R.id.edit_profile_button)
    ImageView editProfileButton;
    @BindView(R.id.welcome_label)
    TextView welcomeLabel;
    @BindView(R.id.date_label)
    TextView dateLabel;
    @BindView(R.id.day_label)
    TextView dayLabel;
    @BindView(R.id.calendar_list)
    RecyclerView calendarList;
    @BindView(R.id.patient_request_list)
    RecyclerView patientRequestList;
    Unbinder unbinder;

    @Inject
    LandingPresenter presenter;
    @BindView(R.id.view_patients_button)
    TextView viewPatientsButton;
    @BindView(R.id.view_consult_button)
    TextView viewConsultButton;
    @BindView(R.id.consult_request_list)
    RecyclerView consultRequestList;


    private ConsultScheduleListAdapter consultScheduleListAdapter;
    private PatientListAdapter patientListAdapter;
    private ConsultListAdapter consultListAdapter;

    public LandingFragment() {
        // Required empty public constructor
    }

    public static LandingFragment newInstance(String message) {
        LandingFragment landingFragment = new LandingFragment();

        Bundle args = new Bundle();
        args.putString("message", message);
        landingFragment.setArguments(args);

        return landingFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        DaggerLandingComponent.builder()
                .requestComponent(((Doc360Application) getActivity().getApplicationContext()).getRequestComponent())
                .landingModule(new LandingModule(this))
                .build().inject(this);
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, view);

        presenter.onViewCreated();

        return view;
    }

    @Override
    public void showLoadingView() {

    }

    @Override
    public void hideLoadingView() {

    }

    @Override
    public void showErrorMessage(String title, String message) {

    }

    @Override
    public void showSuccessMessage(String title, String message) {

    }

    @Override
    public void setupListAdapter() {
        consultScheduleListAdapter = new ConsultScheduleListAdapter();
        calendarList.setLayoutManager(new LinearLayoutManager(getContext()));
        calendarList.setAdapter(consultScheduleListAdapter);

        patientListAdapter = new PatientListAdapter();
        patientRequestList.setLayoutManager(new LinearLayoutManager(getContext()));
        patientRequestList.setAdapter(patientListAdapter);

        patientListAdapter.addList(((HomeActivity) getActivity()).getPatientRequest());

        consultListAdapter = new ConsultListAdapter();
        consultRequestList.setLayoutManager(new LinearLayoutManager(getContext()));
        consultRequestList.setAdapter(consultListAdapter);

        consultListAdapter.addList(((HomeActivity) getActivity()).getConsultRequest());
    }

    @Override
    public void loadDoctorDetails() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy");
        SimpleDateFormat dayFormat = new SimpleDateFormat("EEE", Locale.US);

        dateLabel.setText(dateFormat.format(calendar.getTime()));
        dayLabel.setText(dayFormat.format(calendar.getTime()));
        //welcomeLabel.setText("Welcome Dr. " + );
    }

    @Override
    public void extractBundleData() {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.edit_profile_button, R.id.view_consult_button, R.id.view_patients_button})
    public void onViewClicked(View view) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right, android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        switch (view.getId()) {
            case R.id.edit_profile_button:
                RegistrationFragment registrationFragment = new RegistrationFragment();

                fragmentTransaction.add(android.R.id.content, registrationFragment, "RegistrationFragment");
                fragmentTransaction.addToBackStack("RegistrationFragment");
                fragmentTransaction.commit();
                break;
            case R.id.view_consult_button:
                ConsultFragment consultFragment = new ConsultFragment();
                fragmentTransaction.add(android.R.id.content, consultFragment, "ConsultFragment");
                fragmentTransaction.addToBackStack("ConsultFragment");
                fragmentTransaction.commit();
                break;
            case R.id.view_patients_button:
                PatientRequestFragment patientFragment = new PatientRequestFragment();
                fragmentTransaction.add(android.R.id.content, patientFragment, "PatientRequestFragment");
                fragmentTransaction.addToBackStack("PatientRequestFragment");
                fragmentTransaction.commit();
                break;
        }
    }

    @Override
    public void loadConsultSchedules(List<ConsultItem> consultItemList) {
        consultScheduleListAdapter.addList(consultItemList);
    }

    @Override
    public void loadPatientRequests(List<CareTeamItem> careTeamItems) {
        List<CareTeamItem> loadedList = careTeamItems;

        if (careTeamItems.size() > 3) {
            loadedList = careTeamItems.subList(0, 3);
        }

        patientListAdapter.addList(loadedList);
    }

    @Override
    public void setupUserDetailsView(DoctorProfessionalInfoData doctorProfessionalInfoData) {
        String name = doctorProfessionalInfoData.getFname() + " " + doctorProfessionalInfoData.getLname();
        welcomeLabel.setText("Dr. " + name);

        GlideImageUtil glideImageUtil = new GlideImageUtil(getContext());
        glideImageUtil.setCircularImage(doctorProfessionalInfoData.getPhotolink(), R.drawable.img_default_profile, userImage);
    }

    @Override
    public void loadConsultRequests(List<ConsultRequest> consultRequests) {
        List<ConsultRequest> loadedList = consultRequests;

        if (consultRequests.size() > 3) {
            loadedList = consultRequests.subList(0, 3);
        }


        consultListAdapter.addList(loadedList);
    }
}
