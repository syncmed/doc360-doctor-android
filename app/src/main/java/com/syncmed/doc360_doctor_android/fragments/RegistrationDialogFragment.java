package com.syncmed.doc360_doctor_android.fragments;


import android.app.TimePickerDialog;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.gson.Gson;
import com.syncmed.doc360_doctor_android.Doc360Application;
import com.syncmed.doc360_doctor_android.R;
import com.syncmed.doc360_doctor_android.components.DaggerRegistrationDialogComponent;
import com.syncmed.doc360_doctor_android.interfaces.RegistrationContract;
import com.syncmed.doc360_doctor_android.models.ClinicScheduleItem;
import com.syncmed.doc360_doctor_android.models.DoctorBillingInfoData;
import com.syncmed.doc360_doctor_android.modules.RegistrationDialogModule;
import com.syncmed.doc360_doctor_android.presenters.RegistrationDialogPresenter;

import org.angmarch.views.NiceSpinner;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegistrationDialogFragment extends DialogFragment implements RegistrationContract.DialogView {


    @BindView(R.id.week_spinner)
    NiceSpinner weekSpinner;
    @BindView(R.id.clinic_location_text)
    EditText clinicLocationText;
    @BindView(R.id.from_time_text)
    TextView fromTimeText;
    @BindView(R.id.to_time_text)
    TextView toTimeText;
    @BindView(R.id.time_layout)
    LinearLayout timeLayout;
    @BindView(R.id.clinic_sched_save)
    Button clinicSchedSave;
    @BindView(R.id.voice_video_charge_text)
    EditText voiceVideoChargeText;
    @BindView(R.id.voice_video_charge_layout)
    FrameLayout voiceVideoChargeLayout;
    @BindView(R.id.chat_charge_text)
    EditText chatChargeText;
    @BindView(R.id.consult_charge_layout)
    RelativeLayout consultChargeLayout;
    @BindView(R.id.consult_charge_save)
    Button consultChargeSave;
    @BindView(R.id.chat_charge_layout)
    FrameLayout chatChargeLayout;
    @BindView(R.id.bank_name_text)
    EditText bankNameText;
    @BindView(R.id.acct_type_text)
    EditText acctTypeText;
    @BindView(R.id.acct_no_text)
    EditText acctNoText;
    @BindView(R.id.bank_details_save)
    Button bankDetailsSave;
    @BindView(R.id.bank_details_layout)
    RelativeLayout bankDetailsLayout;
    @BindView(R.id.clinic_schedule_layout)
    RelativeLayout clinicScheduleLayout;
    Unbinder unbinder;

    @Inject
    RegistrationDialogPresenter presenter;

    private RegistrationContract.BillingInfoListener billingInfoListener;
    private RegistrationContract.DoctorRateListener doctorRateListener;
    private RegistrationContract.DoctorScheduleListener doctorScheduleListener;

    private List<String> weekList;
    private Calendar calendar = Calendar.getInstance();
    private TimePickerDialog.OnTimeSetListener fromTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker timePicker, int hour, int minute) {
            calendar.set(Calendar.HOUR, hour);
            calendar.set(Calendar.MINUTE, minute);
            updateFromTimeLabel();
        }
    };
    private TimePickerDialog.OnTimeSetListener toTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker timePicker, int hour, int minute) {
            calendar.set(Calendar.HOUR, hour);
            calendar.set(Calendar.MINUTE, minute);
            updateToTimeLabel();
        }
    };


    public RegistrationDialogFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        DaggerRegistrationDialogComponent.builder()
                .requestComponent(((Doc360Application) getActivity().getApplicationContext()).getRequestComponent())
                .registrationDialogModule(new RegistrationDialogModule(this))
                .build().inject(this);

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_registration_dialog, container, false);
        unbinder = ButterKnife.bind(this, view);

        presenter.onViewCreated();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        Window window = getDialog().getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Point size = new Point();

        Display display = window.getWindowManager().getDefaultDisplay();
        display.getSize(size);

        int width = size.x;
        int height = size.y;
        window.setLayout((int) (width * 0.90), ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
    }

    @Override
    public void showLoadingView() {

    }

    @Override
    public void hideLoadingView() {

    }

    @Override
    public void showErrorMessage(String title, String message) {

    }

    @Override
    public void showSuccessMessage(String title, String message) {

    }

    @Override
    public void setupListAdapter() {
        weekSpinner.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.edit_text_background));
        weekSpinner.setPadding(22, 25, 0, 25);

        weekList = new ArrayList<>();
        weekList.add("Monday");
        weekList.add("Tuesday");
        weekList.add("Wednesday");
        weekList.add("Thursday");
        weekList.add("Friday");

        weekSpinner.attachDataSource(weekList);

        fromTimeText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), fromTimeSetListener, calendar
                        .get(Calendar.HOUR), calendar.get(Calendar.MINUTE),
                        true);
                timePickerDialog.show();
            }
        });

        toTimeText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), toTimeSetListener, calendar
                        .get(Calendar.HOUR), calendar.get(Calendar.MINUTE),
                        true);
                timePickerDialog.show();
            }
        });
    }

    @Override
    public void extractBundleData() {
        Bundle bundle = getArguments();

        if (bundle.getString("action") != null) {
            String action = bundle.getString("action");

            if (action.equals("clinic_schedules")) {
                clinicScheduleLayout.setVisibility(View.VISIBLE);

                if(bundle.getString("clinic_schedules") != null) {
                    Gson gson = new Gson();
                    ClinicScheduleItem item = gson.fromJson(bundle.getString("clinic_schedules"), ClinicScheduleItem.class);

                    int position = weekList.indexOf(item.getDay());
                    weekSpinner.setSelectedIndex(position);
                    clinicLocationText.setText(item.getLocation());

                    String[] timeFrom = item.getTime().split("-");
                    fromTimeText.setText(timeFrom[0]);
                    toTimeText.setText(timeFrom[1]);
                    clinicSchedSave.setTag(item.getId());
                }

                doctorScheduleListener = (RegistrationContract.DoctorScheduleListener) getTargetFragment();

            } else if (action.equals("consult_charges")) {
                consultChargeLayout.setVisibility(View.VISIBLE);

                doctorRateListener = (RegistrationContract.DoctorRateListener) getTargetFragment();

                String chatRate = bundle.getString("chat_rate").replace("\"", "");
                String videoVoiceRate = bundle.getString("video_voice_rate").replace("\"", "");

                chatChargeText.setText(chatRate);
                voiceVideoChargeText.setText(videoVoiceRate);
            } else if (action.equals("bank_details")) {
                bankDetailsLayout.setVisibility(View.VISIBLE);

                billingInfoListener = (RegistrationContract.BillingInfoListener) getTargetFragment();
                Gson gson = new Gson();
                DoctorBillingInfoData data = gson.fromJson(bundle.getString("billing_info"), DoctorBillingInfoData.class);
                bankNameText.setText(data.getBankName());
                acctNoText.setText(data.getAccountNo());
                acctTypeText.setText(data.getAccountType());
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void updateFromTimeLabel() {
        String myFormat = "hh:mm a";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        fromTimeText.setText(sdf.format(calendar.getTime()));
    }

    private void updateToTimeLabel() {
        String myFormat = "hh:mm a";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        toTimeText.setText(sdf.format(calendar.getTime()));
    }

    @Override
    public void saveSchedule(ClinicScheduleItem clinicScheduleItem) {
        doctorScheduleListener.onScheduleSaved(clinicScheduleItem);
        dismissDialog();
    }

    @Override
    public void dismissDialog() {
        dismiss();
    }

    @OnClick({R.id.clinic_sched_save, R.id.consult_charge_save, R.id.bank_details_save})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.clinic_sched_save:
                String timeFrom = fromTimeText.getText().toString() + " - " + toTimeText.getText().toString();

                ClinicScheduleItem clinicScheduleItem = new ClinicScheduleItem();
                clinicScheduleItem.setLocation(clinicLocationText.getText().toString());
                clinicScheduleItem.setTime(timeFrom);
                clinicScheduleItem.setDay(weekSpinner.getText().toString().toLowerCase());

                if(clinicSchedSave.getTag() != null) {
                    clinicScheduleItem.setId(clinicSchedSave.getTag().toString());
                    presenter.updateClinicSchedule(clinicScheduleItem);
                } else {
                    presenter.postClinicSchedule(clinicScheduleItem);
                }


                break;
            case R.id.consult_charge_save:
                doctorRateListener.onRateSaved(
                        voiceVideoChargeText.getText().toString(),
                        chatChargeText.getText().toString());
                dismissDialog();
                break;
            case R.id.bank_details_save:
                DoctorBillingInfoData data = new DoctorBillingInfoData();
                data.setBankName(bankNameText.getText().toString());
                data.setAccountNo(acctNoText.getText().toString());
                data.setAccountType(acctTypeText.getText().toString());

                billingInfoListener.onBillingDataSaved(data);
                dismissDialog();
                break;
        }
    }
}
