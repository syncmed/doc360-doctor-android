package com.syncmed.doc360_doctor_android.components;

import com.syncmed.doc360_doctor_android.activities.HomeActivity;
import com.syncmed.doc360_doctor_android.fragments.LandingFragment;
import com.syncmed.doc360_doctor_android.modules.HomeModule;
import com.syncmed.doc360_doctor_android.modules.LandingModule;
import com.syncmed.doc360_doctor_android.scopes.Activity;

import dagger.Component;

/**
 * Created by ernestgayyed on 15/08/2017.
 */
@Activity
@Component(dependencies = RequestComponent.class, modules = LandingModule.class)
public interface LandingComponent {
    void inject(LandingFragment landingFragment);
}
