package com.syncmed.doc360_doctor_android.components;

import com.syncmed.doc360_doctor_android.helpers.DataManager;
import com.syncmed.doc360_doctor_android.modules.AppModule;
import com.syncmed.doc360_doctor_android.modules.RequestModule;
import com.syncmed.doc360_doctor_android.request.NetworkAPI;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by ernestgayyed on 10/08/2017.
 */

@Singleton
@Component(modules = {AppModule.class, RequestModule.class})
public interface RequestComponent {
    NetworkAPI networkRequest();
    DataManager dataManager();
}
