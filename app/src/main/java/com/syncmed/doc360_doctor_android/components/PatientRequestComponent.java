package com.syncmed.doc360_doctor_android.components;

import com.syncmed.doc360_doctor_android.fragments.PatientFragment;
import com.syncmed.doc360_doctor_android.fragments.PatientRequestFragment;
import com.syncmed.doc360_doctor_android.modules.PatientModule;
import com.syncmed.doc360_doctor_android.modules.PatientRequestModule;
import com.syncmed.doc360_doctor_android.scopes.Activity;

import dagger.Component;

/**
 * Created by ernestgayyed on 15/08/2017.
 */
@Activity
@Component(dependencies = RequestComponent.class, modules = PatientRequestModule.class)
public interface PatientRequestComponent {
    void inject(PatientRequestFragment fragment);
}
