package com.syncmed.doc360_doctor_android.components;

import com.syncmed.doc360_doctor_android.fragments.RegistrationFragment;
import com.syncmed.doc360_doctor_android.modules.RegistrationModule;
import com.syncmed.doc360_doctor_android.scopes.Activity;

import dagger.Component;

/**
 * Created by ernestgayyed on 15/08/2017.
 */
@Activity
@Component(dependencies = RequestComponent.class, modules = RegistrationModule.class)
public interface RegistrationComponent {
    void inject(RegistrationFragment fragment);
}
