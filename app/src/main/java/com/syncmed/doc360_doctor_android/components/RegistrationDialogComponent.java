package com.syncmed.doc360_doctor_android.components;

import com.syncmed.doc360_doctor_android.fragments.RegistrationDialogFragment;
import com.syncmed.doc360_doctor_android.modules.RegistrationDialogModule;
import com.syncmed.doc360_doctor_android.scopes.Activity;

import dagger.Component;

/**
 * Created by ernestgayyed on 15/08/2017.
 */
@Activity
@Component(dependencies = RequestComponent.class, modules = RegistrationDialogModule.class)
public interface RegistrationDialogComponent {
    void inject(RegistrationDialogFragment fragment);
}
