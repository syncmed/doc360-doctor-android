package com.syncmed.doc360_doctor_android.components;

import com.syncmed.doc360_doctor_android.activities.HomeActivity;
import com.syncmed.doc360_doctor_android.activities.LoginActivity;
import com.syncmed.doc360_doctor_android.modules.HomeModule;
import com.syncmed.doc360_doctor_android.modules.LoginModule;
import com.syncmed.doc360_doctor_android.scopes.Activity;

import dagger.Component;

/**
 * Created by ernestgayyed on 15/08/2017.
 */
@Activity
@Component(dependencies = RequestComponent.class, modules = HomeModule.class)
public interface HomeComponent {
    void inject(HomeActivity activity);
}
