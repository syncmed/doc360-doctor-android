package com.syncmed.doc360_doctor_android.components;

import com.syncmed.doc360_doctor_android.MainActivity;
import com.syncmed.doc360_doctor_android.activities.LoginActivity;
import com.syncmed.doc360_doctor_android.modules.LoginModule;
import com.syncmed.doc360_doctor_android.modules.MainModule;
import com.syncmed.doc360_doctor_android.scopes.Activity;

import dagger.Component;

/**
 * Created by ernestgayyed on 15/08/2017.
 */
@Activity
@Component(dependencies = RequestComponent.class, modules = MainModule.class)
public interface MainComponent {
    void inject(MainActivity activity);
}
