package com.syncmed.doc360_doctor_android.components;

import com.syncmed.doc360_doctor_android.activities.HomeActivity;
import com.syncmed.doc360_doctor_android.fragments.PatientFragment;
import com.syncmed.doc360_doctor_android.modules.HomeModule;
import com.syncmed.doc360_doctor_android.modules.PatientModule;
import com.syncmed.doc360_doctor_android.scopes.Activity;

import dagger.Component;

/**
 * Created by ernestgayyed on 15/08/2017.
 */
@Activity
@Component(dependencies = RequestComponent.class, modules = PatientModule.class)
public interface PatientComponent {
    void inject(PatientFragment fragment);
}
