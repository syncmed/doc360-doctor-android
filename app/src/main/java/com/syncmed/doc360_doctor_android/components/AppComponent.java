package com.syncmed.doc360_doctor_android.components;

import com.syncmed.doc360_doctor_android.Doc360Application;
import com.syncmed.doc360_doctor_android.modules.AppModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by ernestgayyed on 10/08/2017.
 */
@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {
    Doc360Application app();
}