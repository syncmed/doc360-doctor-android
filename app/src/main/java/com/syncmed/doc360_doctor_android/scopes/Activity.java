package com.syncmed.doc360_doctor_android.scopes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by ernestgayyed on 10/08/2017.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface Activity {
}
