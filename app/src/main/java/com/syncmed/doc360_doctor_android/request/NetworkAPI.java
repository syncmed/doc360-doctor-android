package com.syncmed.doc360_doctor_android.request;

import com.syncmed.doc360_doctor_android.models.CareTeamPendingResponse;
import com.syncmed.doc360_doctor_android.models.DoctorClinicScheduleRequest;
import com.syncmed.doc360_doctor_android.models.DoctorInfoResponse;
import com.syncmed.doc360_doctor_android.models.DoctorPracticeResponse;
import com.syncmed.doc360_doctor_android.models.DoctorProfileResponse;
import com.syncmed.doc360_doctor_android.models.GenericResponse;
import com.syncmed.doc360_doctor_android.models.LoginRequest;
import com.syncmed.doc360_doctor_android.models.LoginResponse;
import com.syncmed.doc360_doctor_android.models.SpecializationResponse;

import io.reactivex.Observable;
import io.reactivex.Single;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by ernestgayyed on 10/08/2017.
 */

public interface NetworkAPI {

    @POST("doctor/login")
    Call<LoginResponse> loginDoctor(@Body LoginRequest requestBody);

    @POST("doctor/register")
    Call<GenericResponse> registerDoctor(@Body LoginRequest requestBody);

    @GET("docspecialization")
    Call<SpecializationResponse> getSpecialization(@Header("Authorization") String authHeader);

    @GET("doctor/{doctorID}/doctorprofile")
    Call<DoctorProfileResponse> getDoctorInfo(@Path("doctorID") String doctorId, @Header("Authorization") String authHeader);

    @GET("doctor/{doctorID}/doctorpractice")
    Call<DoctorPracticeResponse> getDoctorPractice(@Path("doctorID") String doctorId, @Header("Authorization") String authHeader);

    @DELETE("doctor/{doctorID}/doctorclinicsched/{doctorclinicschedID}")
    Call<ResponseBody> deleteDoctorPracticeSchedule(@Path("doctorID") String doctorId, @Path("doctorclinicschedID") String doctorclinicschedID, @Header("Authorization") String authHeader);

    @POST("doctor/{doctorID}/doctorclinicsched")
    Call<GenericResponse> postClinicSchedule(@Path("doctorID") String doctorId, @Body DoctorClinicScheduleRequest requestBody, @Header("Authorization") String authHeader);

    @PUT("doctor/{doctorID}/doctorclinicsched/{doctorclinicschedID}")
    Call<GenericResponse> updateClinicSchedule(@Path("doctorID") String doctorId, @Path("doctorclinicschedID") String itemId, @Body RequestBody requestBody, @Header("Authorization") String authHeader);

    @PUT("doctor/{doctorID}/doctorbillinfo")
    Call<GenericResponse> updateBillingInfo(@Path("doctorID") String doctorId, @Body RequestBody requestBody, @Header("Authorization") String authHeader);

    @PUT("doctor/{doctorID}/doctorinfo")
    Call<GenericResponse> updateDoctorProfile(@Path("doctorID") String doctorId, @Body RequestBody requestBody, @Header("Authorization") String authHeader);

    @PUT("doctor/{doctorID}/doctorpractice")
    Call<GenericResponse> updateDoctorPractice(@Path("doctorID") String doctorId, @Body RequestBody requestBody, @Header("Authorization") String authHeader);

    @GET("doctor/{doctorID}/careteam/pending")
    Call<CareTeamPendingResponse> getPendingPatientList(@Path("doctorID") String doctorId, @Header("Authorization") String authHeader);

    @GET("doctor/{doctorID}/careteam/approved")
    Call<CareTeamPendingResponse> getPatientList(@Path("doctorID") String doctorId, @Header("Authorization") String authHeader);

    @PUT("doctor/{doctorID}/careteam/{recordID}")
    Call<GenericResponse> approvePatientRequest(@Path("doctorID") String doctorId, @Path("recordID") String recordId, @Body RequestBody requestBody, @Header("Authorization") String authHeader);

    @PUT("user/{userID}/userdevice")
    Call<GenericResponse> updateDeviceToken(@Path("userID") String userID, @Body RequestBody requestBody, @Header("Authorization") String authHeader);
}
