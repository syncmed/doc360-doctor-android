package com.syncmed.doc360_doctor_android.presenters;

import android.util.Log;

import com.google.gson.Gson;
import com.syncmed.doc360_doctor_android.helpers.DataManager;
import com.syncmed.doc360_doctor_android.helpers.Utility;
import com.syncmed.doc360_doctor_android.interfaces.HomeContract;
import com.syncmed.doc360_doctor_android.interfaces.LoginContract;
import com.syncmed.doc360_doctor_android.models.AccessToken;
import com.syncmed.doc360_doctor_android.models.Account;
import com.syncmed.doc360_doctor_android.models.CareTeamPendingResponse;
import com.syncmed.doc360_doctor_android.models.ConsultRequest;
import com.syncmed.doc360_doctor_android.models.DeviceToken;
import com.syncmed.doc360_doctor_android.models.DeviceTokenPutRequest;
import com.syncmed.doc360_doctor_android.models.DoctorProfileData;
import com.syncmed.doc360_doctor_android.models.GenericResponse;
import com.syncmed.doc360_doctor_android.models.LoginRequest;
import com.syncmed.doc360_doctor_android.models.LoginResponse;
import com.syncmed.doc360_doctor_android.models.Patient;
import com.syncmed.doc360_doctor_android.request.NetworkAPI;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ernestgayyed on 29/09/2017.
 */

public class HomePresenter implements HomeContract.Presenter {
    private HomeContract.View view;
    private NetworkAPI networkAPI;
    private DataManager dataManager;

    @Inject
    public HomePresenter(HomeContract.View view, NetworkAPI networkAPI, DataManager dataManager) {
        this.view = view;
        this.networkAPI = networkAPI;
        this.dataManager = dataManager;
    }

    @Override
    public void onViewCreated() {
        view.getDeviceToken();
        view.setupListAdapter();
        loadPatientRequests();
    }

    @Override
    public void refreshData() {

    }

    public void loadPatientRequests() {
        Call<CareTeamPendingResponse> getPendingPatients = networkAPI.getPendingPatientList(dataManager.getLoggedAccount().getRoleId(), dataManager.getAccessToken().getBearerAccessToken());
        getPendingPatients.enqueue(new Callback<CareTeamPendingResponse>() {
            @Override
            public void onResponse(Call<CareTeamPendingResponse> call, Response<CareTeamPendingResponse> response) {
                if(response.isSuccessful()) {
                    if(response.body().getCode().equals("200")) {
                        view.setPatientRequestBadge(String.valueOf(response.body().getData().size()));
                        view.setPatientRequest(response.body().getData());
                        loadConsultRequests();
                    }
                }
            }

            @Override
            public void onFailure(Call<CareTeamPendingResponse> call, Throwable t) {
                Log.d("test", "test");
            }
        });
    }

    public void loadConsultRequests() {
        Patient patient1 = new Patient();
        patient1.setFname("Ernest");
        patient1.setLname("Gayyed");
        patient1.setImageUrl("https://pbs.twimg.com/profile_images/761739039489667073/FoVG4Fu-_400x400.jpg");

        Patient patient2 = new Patient();
        patient2.setFname("Derrick");
        patient2.setLname("Rose");
        patient2.setImageUrl("http://a.espncdn.com/combiner/i?img=/i/headshots/nba/players/full/3456.png&w=350&h=254");

        Patient patient3 = new Patient();
        patient3.setFname("Kobe");
        patient3.setLname("Bryant");
        patient3.setImageUrl("https://b.fssta.com/uploads/application/nba/players/3100.vresize.350.425.medium.46.png");

        Patient patient4 = new Patient();
        patient4.setFname("Lebron");
        patient4.setLname("James");
        patient4.setImageUrl("http://a.espncdn.com/combiner/i?img=/i/headshots/nba/players/full/1966.png&w=350&h=254");

        Patient patient5 = new Patient();
        patient5.setFname("Steph");
        patient5.setLname("Curry");
        patient5.setImageUrl("http://www.slamonline.com/wp-content/uploads/2017/04/curry-2.jpg");

        Patient patient6 = new Patient();
        patient6.setFname("Dwyane");
        patient6.setLname("Wade");
        patient6.setImageUrl("https://upload.wikimedia.org/wikipedia/commons/thumb/7/73/Dwyane_Wade_e1.jpg/1200px-Dwyane_Wade_e1.jpg");


        List<Patient> patients = new ArrayList<>();
        patients.add(patient1);
        patients.add(patient2);
        patients.add(patient3);

        ConsultRequest consultRequest1 = new ConsultRequest();
        consultRequest1.setConsultSchedule("Monday 1am - 3am");
        consultRequest1.setConsultType("Video Consult");
        consultRequest1.setId("1");
        consultRequest1.setPatient(patient1);

        ConsultRequest consultRequest2 = new ConsultRequest();
        consultRequest2.setConsultSchedule("Monday 1am - 3am");
        consultRequest2.setConsultType("Video Consult");
        consultRequest2.setId("2");
        consultRequest2.setPatient(patient2);

        ConsultRequest consultRequest3 = new ConsultRequest();
        consultRequest3.setConsultSchedule("Monday 1am - 3am");
        consultRequest3.setConsultType("Video Consult");
        consultRequest3.setId("3");
        consultRequest3.setPatient(patient3);

        ConsultRequest consultRequest4 = new ConsultRequest();
        consultRequest4.setConsultSchedule("Monday 1am - 3am");
        consultRequest4.setConsultType("Video Consult");
        consultRequest4.setId("4");
        consultRequest4.setPatient(patient4);

        List<ConsultRequest> consultRequests = new ArrayList<>();
        consultRequests.add(consultRequest1);
        consultRequests.add(consultRequest2);
        consultRequests.add(consultRequest3);
        consultRequests.add(consultRequest4);

        //view.setConsultRequestBadge(String.valueOf(consultRequests.size()));
        view.setConsultRequest(consultRequests);
    }

    public void postDeviceToken(String deviceToken) {
        DeviceTokenPutRequest deviceTokenPutRequest = new DeviceTokenPutRequest();
        DeviceToken token = new DeviceToken();
        token.setDeviceToken(deviceToken);

        List<DeviceToken> list = new ArrayList<>();
        list.add(token);

        deviceTokenPutRequest.setDetails(list);

        MediaType mediaType = MediaType.parse("application/octet-stream");
        String requestJson = new Gson().toJson(deviceTokenPutRequest);
        RequestBody requestBody = RequestBody.create(mediaType, requestJson);

        Account account = dataManager.getLoggedAccount();

        Call<GenericResponse> updateDeviceToken = networkAPI.updateDeviceToken(dataManager.getLoggedAccount().getAccountId(), requestBody, dataManager.getAccessToken().getBearerAccessToken());
        updateDeviceToken.enqueue(new Callback<GenericResponse>() {
            @Override
            public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                Log.d("test", "test");
            }

            @Override
            public void onFailure(Call<GenericResponse> call, Throwable t) {
                Log.d("test", "test");
            }
        });
    }
}
