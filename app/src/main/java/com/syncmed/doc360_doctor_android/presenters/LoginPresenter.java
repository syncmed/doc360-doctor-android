package com.syncmed.doc360_doctor_android.presenters;

import android.util.Log;

import com.google.gson.Gson;
import com.syncmed.doc360_doctor_android.helpers.DataManager;
import com.syncmed.doc360_doctor_android.helpers.Utility;
import com.syncmed.doc360_doctor_android.interfaces.LoginContract;
import com.syncmed.doc360_doctor_android.interfaces.RegistrationContract;
import com.syncmed.doc360_doctor_android.models.AccessToken;
import com.syncmed.doc360_doctor_android.models.Account;
import com.syncmed.doc360_doctor_android.models.DeviceToken;
import com.syncmed.doc360_doctor_android.models.DeviceTokenPutRequest;
import com.syncmed.doc360_doctor_android.models.DoctorProfileData;
import com.syncmed.doc360_doctor_android.models.DoctorProfileResponse;
import com.syncmed.doc360_doctor_android.models.GenericResponse;
import com.syncmed.doc360_doctor_android.models.LoginRequest;
import com.syncmed.doc360_doctor_android.models.LoginResponse;
import com.syncmed.doc360_doctor_android.request.NetworkAPI;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ernestgayyed on 29/09/2017.
 */

public class LoginPresenter implements LoginContract.Presenter {
    private LoginContract.View view;
    private NetworkAPI networkAPI;
    private DataManager dataManager;

    @Inject
    public LoginPresenter(LoginContract.View view, NetworkAPI networkAPI, DataManager dataManager) {
        this.view = view;
        this.networkAPI = networkAPI;
        this.dataManager = dataManager;
    }

    @Override
    public void onViewCreated() {
    }

    @Override
    public void refreshData() {

    }

    public void onLoginClicked(final String email, final String password) {
        if(Utility.isEmailValid(email) && Utility.isPasswordValid(password)) {
            LoginRequest registerRequest = new LoginRequest();
            registerRequest.setEmail(email);
            registerRequest.setPassword(password);

            loginDoctor(registerRequest);

//            final LoginRequest loginRequest = registerRequest;
//            Call<GenericResponse> registerDoctor = networkAPI.registerDoctor(registerRequest);
//            registerDoctor.enqueue(new Callback<GenericResponse>() {
//                @Override
//                public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
//                    if(response.isSuccessful()) {
//                        int code = response.code();
//                        if(response.body().getCode().equals("409")) {
//                            view.loginFirebase(email, password);
//                        } else if (response.body().getCode().equals("200")) {
//                            view.registerFirebase(email, password);
//                        }
//
//                        loginDoctor(loginRequest);
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<GenericResponse> call, Throwable t) {
//                    Log.d("test", "test");
//                }
//            });
        }

    }

    public void loginDoctor(final LoginRequest loginRequest) {
        view.showLoadingView();
        Call<LoginResponse> loginDoctor = networkAPI.loginDoctor(loginRequest);
        loginDoctor.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                view.hideLoadingView();
                if(response.isSuccessful()) {
                    if(response.body().getCode().equals("200")) {
                        view.loginFirebase(loginRequest.getEmail(), loginRequest.getPassword());

                        AccessToken userAccessToken = new AccessToken();
                        userAccessToken.setBearerAccessToken(response.body().getData().getAccessToken());
                        dataManager.setAccessToken(userAccessToken);

                        Account account = new Account();
                        account.setAccountId(response.body().getData().getUserID());
                        account.setRoleId(response.body().getData().getDistinctID());
                        account.setRole("doctor");

                        dataManager.setLoggedAccount(account);
                        view.presentRegistrationView();
                    } else {
                        view.showErrorMessage("Login", "Error logging in");
                    }
                } else {
                    view.showErrorMessage("Login", "Error logging in");
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                view.hideLoadingView();
                view.showErrorMessage("Login", "Error logging in");
            }
        });
    }
}
