package com.syncmed.doc360_doctor_android.presenters;

import com.syncmed.doc360_doctor_android.helpers.DataManager;
import com.syncmed.doc360_doctor_android.interfaces.ConsultContract;
import com.syncmed.doc360_doctor_android.interfaces.PatientContract;
import com.syncmed.doc360_doctor_android.models.ConsultRequest;
import com.syncmed.doc360_doctor_android.models.Patient;
import com.syncmed.doc360_doctor_android.request.NetworkAPI;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by ernestgayyed on 29/09/2017.
 */

public class ConsultPresenter implements ConsultContract.Presenter {
    private ConsultContract.View view;
    private NetworkAPI networkAPI;
    private DataManager dataManager;

    @Inject
    public ConsultPresenter(ConsultContract.View view, NetworkAPI networkAPI, DataManager dataManager) {
        this.view = view;
        this.networkAPI = networkAPI;
        this.dataManager = dataManager;
    }

    @Override
    public void onViewCreated() {
        view.setupListAdapter();
        loadConsultRequests();
    }

    @Override
    public void refreshData() {

    }

    public void loadConsultRequests() {
        Patient patient1 = new Patient();
        patient1.setFname("Ernest");
        patient1.setLname("Gayyed");
        patient1.setImageUrl("https://pbs.twimg.com/profile_images/761739039489667073/FoVG4Fu-_400x400.jpg");

        Patient patient2 = new Patient();
        patient2.setFname("Derrick");
        patient2.setLname("Rose");
        patient2.setImageUrl("http://a.espncdn.com/combiner/i?img=/i/headshots/nba/players/full/3456.png&w=350&h=254");

        Patient patient3 = new Patient();
        patient3.setFname("Kobe");
        patient3.setLname("Bryant");
        patient3.setImageUrl("https://b.fssta.com/uploads/application/nba/players/3100.vresize.350.425.medium.46.png");

        Patient patient4 = new Patient();
        patient4.setFname("Lebron");
        patient4.setLname("James");
        patient4.setImageUrl("http://a.espncdn.com/combiner/i?img=/i/headshots/nba/players/full/1966.png&w=350&h=254");

        Patient patient5 = new Patient();
        patient5.setFname("Steph");
        patient5.setLname("Curry");
        patient5.setImageUrl("http://www.slamonline.com/wp-content/uploads/2017/04/curry-2.jpg");

        Patient patient6 = new Patient();
        patient6.setFname("Dwyane");
        patient6.setLname("Wade");
        patient6.setImageUrl("https://upload.wikimedia.org/wikipedia/commons/thumb/7/73/Dwyane_Wade_e1.jpg/1200px-Dwyane_Wade_e1.jpg");


        List<Patient> patients = new ArrayList<>();
        patients.add(patient1);
        patients.add(patient2);
        patients.add(patient3);

        ConsultRequest consultRequest1 = new ConsultRequest();
        consultRequest1.setConsultSchedule("Monday 1am - 3am");
        consultRequest1.setConsultType("Video Consult");
        consultRequest1.setId("1");
        consultRequest1.setPatient(patient1);

        ConsultRequest consultRequest2 = new ConsultRequest();
        consultRequest2.setConsultSchedule("Monday 1am - 3am");
        consultRequest2.setConsultType("Video Consult");
        consultRequest2.setId("2");
        consultRequest2.setPatient(patient2);

        ConsultRequest consultRequest3 = new ConsultRequest();
        consultRequest3.setConsultSchedule("Monday 1am - 3am");
        consultRequest3.setConsultType("Video Consult");
        consultRequest3.setId("3");
        consultRequest3.setPatient(patient3);

        ConsultRequest consultRequest4 = new ConsultRequest();
        consultRequest4.setConsultSchedule("Monday 1am - 3am");
        consultRequest4.setConsultType("Video Consult");
        consultRequest4.setId("4");
        consultRequest4.setPatient(patient4);

        List<ConsultRequest> consultRequests = new ArrayList<>();
        consultRequests.add(consultRequest1);
        consultRequests.add(consultRequest2);
        consultRequests.add(consultRequest3);
        consultRequests.add(consultRequest4);

        view.loadConsultRequestList(consultRequests);
        view.reloadConsultRequestBadge(String.valueOf(consultRequests.size()));
    }
}
