package com.syncmed.doc360_doctor_android.presenters;

import android.util.Log;

import com.google.gson.Gson;
import com.syncmed.doc360_doctor_android.helpers.DataManager;
import com.syncmed.doc360_doctor_android.interfaces.HomeContract;
import com.syncmed.doc360_doctor_android.interfaces.PatientContract;
import com.syncmed.doc360_doctor_android.models.CareTeamPendingResponse;
import com.syncmed.doc360_doctor_android.models.GenericResponse;
import com.syncmed.doc360_doctor_android.models.Patient;
import com.syncmed.doc360_doctor_android.models.PatientApprovalItem;
import com.syncmed.doc360_doctor_android.models.PatientApprovalRequest;
import com.syncmed.doc360_doctor_android.request.NetworkAPI;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ernestgayyed on 29/09/2017.
 */

public class PatientPresenter implements PatientContract.Presenter {
    private PatientContract.View view;
    private NetworkAPI networkAPI;
    private DataManager dataManager;

    @Inject
    public PatientPresenter(PatientContract.View view, NetworkAPI networkAPI, DataManager dataManager) {
        this.view = view;
        this.networkAPI = networkAPI;
        this.dataManager = dataManager;
    }

    @Override
    public void onViewCreated() {
        view.setupListAdapter();
        loadPatientList();
        loadPatientRequests();
    }

    @Override
    public void refreshData() {

    }

    public void loadPatientList() {
        Call<CareTeamPendingResponse> getPendingPatients = networkAPI.getPatientList(dataManager.getLoggedAccount().getRoleId(), dataManager.getAccessToken().getBearerAccessToken());
        getPendingPatients.enqueue(new Callback<CareTeamPendingResponse>() {
            @Override
            public void onResponse(Call<CareTeamPendingResponse> call, Response<CareTeamPendingResponse> response) {
                if(response.isSuccessful()) {
                    if(response.body().getCode().equals("200")) {
                        view.loadPatientList(response.body().getData());
                    }
                }
            }

            @Override
            public void onFailure(Call<CareTeamPendingResponse> call, Throwable t) {
                Log.d("test", "test");
            }
        });
    }

    public void loadPatientRequests() {
        Call<CareTeamPendingResponse> getPendingPatients = networkAPI.getPendingPatientList(dataManager.getLoggedAccount().getRoleId(), dataManager.getAccessToken().getBearerAccessToken());
        getPendingPatients.enqueue(new Callback<CareTeamPendingResponse>() {
            @Override
            public void onResponse(Call<CareTeamPendingResponse> call, Response<CareTeamPendingResponse> response) {
                if(response.isSuccessful()) {
                    if(response.body().getCode().equals("200")) {
                        //view.loadPatientRequestList(response.body().getData());
                        view.reloadPatientRequestBadge(String.valueOf(response.body().getData().size()));
                    }
                }
            }

            @Override
            public void onFailure(Call<CareTeamPendingResponse> call, Throwable t) {
                Log.d("test", "test");
            }
        });
    }

    public void postPatientRequest(String recordId, final String isApproved) {
        view.showLoadingView();
        PatientApprovalRequest patientApprovalRequest = new PatientApprovalRequest();
        PatientApprovalItem patientApprovalItem = new PatientApprovalItem();
        patientApprovalItem.setAction(isApproved);

        List<PatientApprovalItem> list = new ArrayList<>();
        list.add(patientApprovalItem);

        patientApprovalRequest.setCareteam(list);

        MediaType mediaType = MediaType.parse("application/octet-stream");
        String requestJson = new Gson().toJson(patientApprovalRequest);
        RequestBody requestBody = RequestBody.create(mediaType, requestJson);

        Call<GenericResponse> approvePatientRequest = networkAPI.approvePatientRequest(dataManager.getLoggedAccount().getRoleId(), recordId, requestBody, dataManager.getAccessToken().getBearerAccessToken());
        approvePatientRequest.enqueue(new Callback<GenericResponse>() {
            @Override
            public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                view.hideLoadingView();
                if(response.isSuccessful()) {
                    if(response.body().getCode().equals("200")) {
                        if(isApproved.equals("1")) {
                            view.showSuccessMessage("Approval Request", "Patient approved successfully!");
                        } else {
                            view.showSuccessMessage("Approval Request", "You declined the patient");
                        }

                        loadPatientRequests();
                    }
                }
            }

            @Override
            public void onFailure(Call<GenericResponse> call, Throwable t) {
                view.hideLoadingView();
            }
        });
    }
}
