package com.syncmed.doc360_doctor_android.presenters;

import android.util.Log;

import com.syncmed.doc360_doctor_android.helpers.DataManager;
import com.syncmed.doc360_doctor_android.helpers.Utility;
import com.syncmed.doc360_doctor_android.interfaces.LoginContract;
import com.syncmed.doc360_doctor_android.interfaces.RegisterContract;
import com.syncmed.doc360_doctor_android.models.AccessToken;
import com.syncmed.doc360_doctor_android.models.Account;
import com.syncmed.doc360_doctor_android.models.DoctorProfileData;
import com.syncmed.doc360_doctor_android.models.DoctorProfileResponse;
import com.syncmed.doc360_doctor_android.models.GenericResponse;
import com.syncmed.doc360_doctor_android.models.LoginRequest;
import com.syncmed.doc360_doctor_android.models.LoginResponse;
import com.syncmed.doc360_doctor_android.request.NetworkAPI;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ernestgayyed on 29/09/2017.
 */

public class RegisterPresenter implements RegisterContract.Presenter {
    private RegisterContract.View view;
    private NetworkAPI networkAPI;
    private DataManager dataManager;

    @Inject
    public RegisterPresenter(RegisterContract.View view, NetworkAPI networkAPI, DataManager dataManager) {
        this.view = view;
        this.networkAPI = networkAPI;
        this.dataManager = dataManager;
    }

    @Override
    public void onViewCreated() {
    }

    @Override
    public void refreshData() {

    }

    public void onRegisterClicked(final String email, final String password, String cPassword) {
        if(!password.equals(cPassword)) {
            view.showErrorMessage("Register", "Password is not equal");
        } else {
            view.showLoadingView();
            if(Utility.isEmailValid(email) && Utility.isPasswordValid(password)) {
                LoginRequest registerRequest = new LoginRequest();
                registerRequest.setEmail(email);
                registerRequest.setPassword(password);

                final LoginRequest loginRequest = registerRequest;
                Call<GenericResponse> registerDoctor = networkAPI.registerDoctor(registerRequest);
                registerDoctor.enqueue(new Callback<GenericResponse>() {
                    @Override
                    public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                        if(response.isSuccessful()) {
                            int code = response.code();
                            if(response.body().getCode().equals("409")) {
                                view.loginFirebase(email, password);
                            } else if (response.body().getCode().equals("200")) {
                                view.registerFirebase(email, password);
                            }

                            loginDoctor(loginRequest);
                        }
                    }

                    @Override
                    public void onFailure(Call<GenericResponse> call, Throwable t) {
                        Log.d("test", "test");
                        view.hideLoadingView();
                        view.showErrorMessage("Register", "Error in registration");
                    }
                });
            } else {
                view.hideLoadingView();
                view.showErrorMessage("Register", "Email or Password is invalid");
            }
        }
    }

    public void loginDoctor(LoginRequest loginRequest) {
        Call<LoginResponse> loginDoctor = networkAPI.loginDoctor(loginRequest);
        loginDoctor.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                view.hideLoadingView();
                if(response.isSuccessful()) {
                    AccessToken userAccessToken = new AccessToken();
                    userAccessToken.setBearerAccessToken(response.body().getData().getAccessToken());
                    dataManager.setAccessToken(userAccessToken);

                    Account account = new Account();
                    account.setAccountId(response.body().getData().getUserID());
                    account.setRoleId(response.body().getData().getDistinctID());
                    account.setRole("doctor");

                    dataManager.setLoggedAccount(account);

                    view.presentRegistrationView();
                } else {
                    view.showErrorMessage("Register", "Error in registration.");
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.d("test", "test");
                view.hideLoadingView();
                view.showErrorMessage("Register", "Error in registration.");
            }
        });
    }
}