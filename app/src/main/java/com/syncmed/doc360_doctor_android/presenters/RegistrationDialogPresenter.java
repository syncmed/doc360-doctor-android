package com.syncmed.doc360_doctor_android.presenters;

import com.google.gson.Gson;
import com.syncmed.doc360_doctor_android.helpers.DataManager;
import com.syncmed.doc360_doctor_android.interfaces.RegistrationContract;
import com.syncmed.doc360_doctor_android.models.ClinicScheduleItem;
import com.syncmed.doc360_doctor_android.models.DoctorBillingInfoData;
import com.syncmed.doc360_doctor_android.models.DoctorBillingInfoPutRequest;
import com.syncmed.doc360_doctor_android.models.DoctorClinicScheduleRequest;
import com.syncmed.doc360_doctor_android.models.GenericResponse;
import com.syncmed.doc360_doctor_android.request.NetworkAPI;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ernestgayyed on 29/09/2017.
 */

public class RegistrationDialogPresenter implements RegistrationContract.DialogPresenter {
    private RegistrationContract.DialogView view;
    private NetworkAPI networkAPI;
    private DataManager dataManager;

    @Inject
    public RegistrationDialogPresenter(RegistrationContract.DialogView view, NetworkAPI networkAPI, DataManager dataManager) {
        this.view = view;
        this.networkAPI = networkAPI;
        this.dataManager = dataManager;
    }

    @Override
    public void onViewCreated() {
        view.setupListAdapter();
        view.extractBundleData();
    }

    @Override
    public void refreshData() {

    }

    public void postClinicSchedule(final ClinicScheduleItem clinicScheduleItem) {
        List<ClinicScheduleItem> scheduleItemList = new ArrayList<>();
        scheduleItemList.add(clinicScheduleItem);

        final DoctorClinicScheduleRequest request = new DoctorClinicScheduleRequest();
        request.setDoctordetails(scheduleItemList);

        Call<GenericResponse> postClinicSchedule = networkAPI.postClinicSchedule(dataManager.getLoggedAccount().getRoleId(), request, dataManager.getAccessToken().getBearerAccessToken());
        postClinicSchedule.enqueue(new Callback<GenericResponse>() {
            @Override
            public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                if (response.isSuccessful()) {
                    view.saveSchedule(clinicScheduleItem);
                }
            }

            @Override
            public void onFailure(Call<GenericResponse> call, Throwable t) {

            }
        });
    }

    public void updateClinicSchedule(final ClinicScheduleItem clinicScheduleItem) {
        List<ClinicScheduleItem> scheduleItemList = new ArrayList<>();
        scheduleItemList.add(clinicScheduleItem);

        final DoctorClinicScheduleRequest request = new DoctorClinicScheduleRequest();
        request.setDoctordetails(scheduleItemList);

        MediaType mediaType = MediaType.parse("application/octet-stream");
        Gson gson = new Gson();
        //Updating of allergies
        RequestBody requestBody = RequestBody.create(mediaType, gson.toJson(request));

        Call<GenericResponse> postClinicSchedule = networkAPI.updateClinicSchedule(dataManager.getLoggedAccount().getRoleId(), clinicScheduleItem.getId(), requestBody, dataManager.getAccessToken().getBearerAccessToken());
        postClinicSchedule.enqueue(new Callback<GenericResponse>() {
            @Override
            public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                if (response.isSuccessful()) {
                    view.saveSchedule(clinicScheduleItem);
                }
            }

            @Override
            public void onFailure(Call<GenericResponse> call, Throwable t) {

            }
        });
    }

    public void updateBankDetails(String bankName, String accountType, String accountNo) {
        DoctorBillingInfoData data = new DoctorBillingInfoData();
        data.setBankName(bankName);
        data.setAccountNo(accountNo);
        data.setAccountType(accountType);

        List<DoctorBillingInfoData> list = new ArrayList<>();
        list.add(data);

        DoctorBillingInfoPutRequest request = new DoctorBillingInfoPutRequest();
        request.setDoctordetails(list);

        MediaType mediaType = MediaType.parse("application/octet-stream");
        Gson gson = new Gson();
        //Updating of allergies
        RequestBody requestBody = RequestBody.create(mediaType, gson.toJson(request));

        Call<GenericResponse> updateBankDetails = networkAPI.updateBillingInfo(dataManager.getLoggedAccount().getRoleId(), requestBody, dataManager.getAccessToken().getBearerAccessToken());
        updateBankDetails.enqueue(new Callback<GenericResponse>() {
            @Override
            public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                if(response.isSuccessful()) {
                    if(response.body().getCode().equals("200")) {
                        //Success
                        view.dismissDialog();
                    }
                }
            }

            @Override
            public void onFailure(Call<GenericResponse> call, Throwable t) {

            }
        });
    }
}
