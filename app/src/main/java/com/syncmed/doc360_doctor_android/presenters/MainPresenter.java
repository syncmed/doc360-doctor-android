package com.syncmed.doc360_doctor_android.presenters;

import android.util.Log;

import com.google.gson.Gson;
import com.syncmed.doc360_doctor_android.helpers.DataManager;
import com.syncmed.doc360_doctor_android.interfaces.LandingContract;
import com.syncmed.doc360_doctor_android.interfaces.MainContract;
import com.syncmed.doc360_doctor_android.models.AccessToken;
import com.syncmed.doc360_doctor_android.models.Account;
import com.syncmed.doc360_doctor_android.models.ConsultItem;
import com.syncmed.doc360_doctor_android.models.DeviceToken;
import com.syncmed.doc360_doctor_android.models.DeviceTokenPutRequest;
import com.syncmed.doc360_doctor_android.models.DoctorProfileData;
import com.syncmed.doc360_doctor_android.models.DoctorProfileResponse;
import com.syncmed.doc360_doctor_android.models.GenericResponse;
import com.syncmed.doc360_doctor_android.models.Patient;
import com.syncmed.doc360_doctor_android.request.NetworkAPI;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ernestgayyed on 29/09/2017.
 */

public class MainPresenter implements MainContract.Presenter {
    private MainContract.View view;
    private NetworkAPI networkAPI;
    private DataManager dataManager;

    @Inject
    public MainPresenter(MainContract.View view, NetworkAPI networkAPI, DataManager dataManager) {
        this.view = view;
        this.networkAPI = networkAPI;
        this.dataManager = dataManager;
    }

    @Override
    public void onViewCreated() {
        view.setupListAdapter();
        Account account = dataManager.getLoggedAccount();
        AccessToken accessToken = dataManager.getAccessToken();
        DoctorProfileData doctorProfileData = dataManager.getLoggedDoctorProfile();

        if(account != null && accessToken != null) {
            view.showLoadingView();
            validateUserCredentials();
        }
    }

    @Override
    public void refreshData() {
    }



    public void validateUserCredentials() {
        Call<DoctorProfileResponse> getProfile = networkAPI.getDoctorInfo(dataManager.getLoggedAccount().getRoleId(), dataManager.getAccessToken().getBearerAccessToken());
        getProfile.enqueue(new Callback<DoctorProfileResponse>() {
            @Override
            public void onResponse(Call<DoctorProfileResponse> call, Response<DoctorProfileResponse> response) {
                if(response.isSuccessful()) {
                    view.hideLoadingView();
                    if(response.body().getCode().equals("200")) {
                        DoctorProfileData doctorProfileData = response.body().getData().get(0);
                        dataManager.setLoggedDoctorProfile(doctorProfileData);
                        view.getDeviceToken();
                        view.presentHomeView();
                    }
                }
            }

            @Override
            public void onFailure(Call<DoctorProfileResponse> call, Throwable t) {
                dataManager.removeLoggedDoctorProfile();
                view.hideLoadingView();
            }
        });
    }

    public void postDeviceToken(String deviceToken) {
        DeviceTokenPutRequest deviceTokenPutRequest = new DeviceTokenPutRequest();
        DeviceToken token = new DeviceToken();
        token.setDeviceToken(deviceToken);

        List<DeviceToken> list = new ArrayList<>();
        list.add(token);

        deviceTokenPutRequest.setDetails(list);

        MediaType mediaType = MediaType.parse("application/octet-stream");
        String requestJson = new Gson().toJson(deviceTokenPutRequest);
        RequestBody requestBody = RequestBody.create(mediaType, requestJson);

        AccessToken accessToken = dataManager.getAccessToken();

        Call<GenericResponse> updateDeviceToken = networkAPI.updateDeviceToken(dataManager.getLoggedAccount().getRoleId(), requestBody, dataManager.getAccessToken().getBearerAccessToken());
        updateDeviceToken.enqueue(new Callback<GenericResponse>() {
            @Override
            public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                Log.d("test", "test");
            }

            @Override
            public void onFailure(Call<GenericResponse> call, Throwable t) {
                Log.d("test", "test");
            }
        });
    }
}
