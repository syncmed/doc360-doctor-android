package com.syncmed.doc360_doctor_android.presenters;

import android.util.Log;

import com.google.gson.Gson;
import com.syncmed.doc360_doctor_android.helpers.DataManager;
import com.syncmed.doc360_doctor_android.helpers.Utility;
import com.syncmed.doc360_doctor_android.interfaces.RegistrationContract;
import com.syncmed.doc360_doctor_android.models.AccessToken;
import com.syncmed.doc360_doctor_android.models.Account;
import com.syncmed.doc360_doctor_android.models.ClinicScheduleItem;
import com.syncmed.doc360_doctor_android.models.ClinicScheduleList;
import com.syncmed.doc360_doctor_android.models.DoctorBillInfoRequest;
import com.syncmed.doc360_doctor_android.models.DoctorBillingInfoData;
import com.syncmed.doc360_doctor_android.models.DoctorDetailRequest;
import com.syncmed.doc360_doctor_android.models.DoctorDetailsDataRequest;
import com.syncmed.doc360_doctor_android.models.DoctorInfoResponse;
import com.syncmed.doc360_doctor_android.models.DoctorPracticeData;
import com.syncmed.doc360_doctor_android.models.DoctorPracticeInfoData;
import com.syncmed.doc360_doctor_android.models.DoctorPracticeInfoRequest;
import com.syncmed.doc360_doctor_android.models.DoctorPracticeResponse;
import com.syncmed.doc360_doctor_android.models.DoctorProfileData;
import com.syncmed.doc360_doctor_android.models.DoctorProfileResponse;
import com.syncmed.doc360_doctor_android.models.GenericResponse;
import com.syncmed.doc360_doctor_android.models.Specialization;
import com.syncmed.doc360_doctor_android.models.SpecializationItem;
import com.syncmed.doc360_doctor_android.models.SpecializationResponse;
import com.syncmed.doc360_doctor_android.request.NetworkAPI;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ernestgayyed on 28/09/2017.
 */

public class RegistrationPresenter implements RegistrationContract.Presenter {
    private RegistrationContract.View view;
    private NetworkAPI networkAPI;
    private DataManager dataManager;
    private Gson gson = new Gson();

    @Inject
    public RegistrationPresenter(RegistrationContract.View view, NetworkAPI networkAPI, DataManager dataManager) {
        this.view = view;
        this.networkAPI = networkAPI;
        this.dataManager = dataManager;
    }

    @Override
    public void onViewCreated() {
        view.setupListAdapter();
        loadDoctorInfo();
    }

    @Override
    public void refreshData() {

    }

    public void loadDoctorInfo() {
        Account account = dataManager.getLoggedAccount();
        AccessToken accessToken = dataManager.getAccessToken();
        Call<DoctorProfileResponse> getDoctorInfo = networkAPI.getDoctorInfo(dataManager.getLoggedAccount().getRoleId(), dataManager.getAccessToken().getBearerAccessToken());
        getDoctorInfo.enqueue(new Callback<DoctorProfileResponse>() {
            @Override
            public void onResponse(Call<DoctorProfileResponse> call, Response<DoctorProfileResponse> response) {
                if(response.isSuccessful()) {
                    view.loadDoctorProfileData(response.body().getData().get(0));
                    view.presentDoctorProfessionalInfo(response.body().getData().get(0).getProfessionalInfo());
                    view.presentDoctorBillingInfo(response.body().getData().get(0).getBillInfo());
                    view.presentDoctorPracticeInfo(response.body().getData().get(0).getPracticeInfo());
                }
            }

            @Override
            public void onFailure(Call<DoctorProfileResponse> call, Throwable t) {
                Log.d("test", "test");
            }
        });
    }

    public void searchSpecialization(String searchText) {
        if(!Utility.isStringEmptyOrNull(searchText)) {
            Call<SpecializationResponse> getSpecialization = networkAPI.getSpecialization(dataManager.getAccessToken().getBearerAccessToken());
            getSpecialization.enqueue(new Callback<SpecializationResponse>() {
                @Override
                public void onResponse(Call<SpecializationResponse> call, Response<SpecializationResponse> response) {
                    if(response.isSuccessful()) {
                        view.loadSpecializationList(response.body().getData().getSpecialization());
                    }
                }

                @Override
                public void onFailure(Call<SpecializationResponse> call, Throwable t) {
                    SpecializationItem specializationItem1 = new SpecializationItem();
                    specializationItem1.setId("1");
                    specializationItem1.setDesc("ALLERGOLOGY");

                    SpecializationItem specializationItem2 = new SpecializationItem();
                    specializationItem2.setId("2");
                    specializationItem2.setDesc("ANESTHESIOLOGY");

                    SpecializationItem specializationItem3 = new SpecializationItem();
                    specializationItem3.setId("3");
                    specializationItem3.setDesc("ANTERIOR SEGMENT");

                    SpecializationItem specializationItem4 = new SpecializationItem();
                    specializationItem4.setId("4");
                    specializationItem4.setDesc("CANCER");

                    List<SpecializationItem> specializationItems = new ArrayList<SpecializationItem>();
                    specializationItems.add(specializationItem1);
                    specializationItems.add(specializationItem2);
                    specializationItems.add(specializationItem3);
                    specializationItems.add(specializationItem4);

                    view.loadSpecializationList(specializationItems);
                }
            });
        } else {
            view.clearSearchList();
        }
    }

    public void getDoctorPractice() {
        Call<DoctorPracticeResponse> getDoctorPractice = networkAPI.getDoctorPractice(dataManager.getLoggedAccount().getRoleId(), dataManager.getAccessToken().getBearerAccessToken());
        getDoctorPractice.enqueue(new Callback<DoctorPracticeResponse>() {
            @Override
            public void onResponse(Call<DoctorPracticeResponse> call, Response<DoctorPracticeResponse> response) {
                if(response.isSuccessful()) {
                    view.loadClinicSchedule(response.body().getData().get(0).getSchedule());
                }
            }

            @Override
            public void onFailure(Call<DoctorPracticeResponse> call, Throwable t) {

            }
        });
    }

    public void deleteDoctorSchedule(String id) {
        if(!Utility.isStringEmptyOrNull(id)) {
            Call<ResponseBody> deleteSchedule = networkAPI.deleteDoctorPracticeSchedule(dataManager.getLoggedAccount().getRoleId(), id, dataManager.getAccessToken().getBearerAccessToken());
            deleteSchedule.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });
        }
    }

    public void onSaveClicked(final DoctorProfileData doctorProfileData) {
        view.showLoadingView();

        MediaType mediaType = MediaType.parse("application/octet-stream");

        DoctorDetailRequest doctorDetailRequest = new DoctorDetailRequest();
        DoctorDetailsDataRequest doctorDetailsDataRequest = new DoctorDetailsDataRequest();
        doctorDetailsDataRequest.setPhotolink(doctorProfileData.getProfessionalInfo().getPhotolink());
        doctorDetailsDataRequest.setContact(doctorProfileData.getProfessionalInfo().getContact());
        doctorDetailsDataRequest.setEducation(doctorProfileData.getProfessionalInfo().getProfessionalInfo().getEducation());
        doctorDetailsDataRequest.setAward(doctorProfileData.getProfessionalInfo().getProfessionalInfo().getAward());
        doctorDetailsDataRequest.setCertification(doctorProfileData.getProfessionalInfo().getProfessionalInfo().getCertification());

        List<String> specializationList = new ArrayList<>();
        for(Specialization specialization : doctorProfileData.getProfessionalInfo().getSpecializationList()) {
            specializationList.add(specialization.getSpecializationID());
        }

        doctorDetailsDataRequest.setSpecialization(specializationList);

        List<DoctorDetailsDataRequest> doctorDetailsDataRequests = new ArrayList<>();
        doctorDetailsDataRequests.add(doctorDetailsDataRequest);

        doctorDetailRequest.setDoctordetails(doctorDetailsDataRequests);

        String requestJson = gson.toJson(doctorDetailRequest);
        RequestBody requestBody = RequestBody.create(mediaType, requestJson);

        Call<GenericResponse> updateDoctorData = networkAPI.updateDoctorProfile(dataManager.getLoggedAccount().getRoleId(), requestBody, dataManager.getAccessToken().getBearerAccessToken());
        updateDoctorData.enqueue(new Callback<GenericResponse>() {
            @Override
            public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {

                DoctorBillInfoRequest doctorBillInfoRequest = new DoctorBillInfoRequest();
                List<DoctorBillingInfoData> doctorBillingInfoDatas = new ArrayList<>();
                doctorBillingInfoDatas.add(doctorProfileData.getBillInfo());
                doctorBillInfoRequest.setDoctordetails(doctorBillingInfoDatas);

                String billingRequest = gson.toJson(doctorBillInfoRequest);
                MediaType mediaType = MediaType.parse("application/octet-stream");
                RequestBody billingRequestBody = RequestBody.create(mediaType, billingRequest);

                Call<GenericResponse> updateBillingInfo = networkAPI.updateBillingInfo(dataManager.getLoggedAccount().getRoleId(), billingRequestBody, dataManager.getAccessToken().getBearerAccessToken());
                updateBillingInfo.enqueue(new Callback<GenericResponse>() {
                    @Override
                    public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {

                        DoctorPracticeInfoRequest doctorPracticeInfoRequest = new DoctorPracticeInfoRequest();
                        List<DoctorPracticeInfoData> doctorPracticeDatas = new ArrayList<>();
                        doctorPracticeDatas.add(doctorProfileData.getPracticeInfo());
                        doctorPracticeInfoRequest.setDoctordetails(doctorPracticeDatas);

                        String practiceRequest = gson.toJson(doctorPracticeInfoRequest);
                        MediaType mediaType = MediaType.parse("application/octet-stream");
                        RequestBody practiceRequestBody = RequestBody.create(mediaType, practiceRequest);

                        Call<GenericResponse> updatePracticeInfo = networkAPI.updateDoctorPractice(dataManager.getLoggedAccount().getRoleId(), practiceRequestBody, dataManager.getAccessToken().getBearerAccessToken());
                        updatePracticeInfo.enqueue(new Callback<GenericResponse>() {
                            @Override
                            public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                                view.hideLoadingView();
                                if(dataManager.getLoggedDoctorProfile() == null) {
                                    view.presentHomeView();
                                } else {
                                    view.showSuccessMessage("Edit Profile", "Saved successfully!");
                                }
                                dataManager.setLoggedDoctorProfile(doctorProfileData);
                            }

                            @Override
                            public void onFailure(Call<GenericResponse> call, Throwable t) {
                                view.hideLoadingView();
                            }
                        });
                    }

                    @Override
                    public void onFailure(Call<GenericResponse> call, Throwable t) {
                        view.hideLoadingView();
                    }
                });
            }

            @Override
            public void onFailure(Call<GenericResponse> call, Throwable t) {
                view.hideLoadingView();
            }
        });
    }
}
