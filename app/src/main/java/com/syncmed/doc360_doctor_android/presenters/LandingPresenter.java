package com.syncmed.doc360_doctor_android.presenters;

import com.syncmed.doc360_doctor_android.helpers.DataManager;
import com.syncmed.doc360_doctor_android.interfaces.HomeContract;
import com.syncmed.doc360_doctor_android.interfaces.LandingContract;
import com.syncmed.doc360_doctor_android.models.ConsultItem;
import com.syncmed.doc360_doctor_android.models.DoctorProfileData;
import com.syncmed.doc360_doctor_android.models.Patient;
import com.syncmed.doc360_doctor_android.request.NetworkAPI;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by ernestgayyed on 29/09/2017.
 */

public class LandingPresenter implements LandingContract.Presenter {
    private LandingContract.View view;
    private NetworkAPI networkAPI;
    private DataManager dataManager;

    @Inject
    public LandingPresenter(LandingContract.View view, NetworkAPI networkAPI, DataManager dataManager) {
        this.view = view;
        this.networkAPI = networkAPI;
        this.dataManager = dataManager;
    }

    @Override
    public void onViewCreated() {
        view.setupListAdapter();
        view.setupUserDetailsView(dataManager.getLoggedDoctorProfile().getProfessionalInfo());
        loadConsultSchedules();
    }

    @Override
    public void refreshData() {
        DoctorProfileData doctorProfileData = dataManager.getLoggedDoctorProfile();
        view.setupUserDetailsView(doctorProfileData.getProfessionalInfo());
    }

    public void loadConsultSchedules() {
        Patient patient1 = new Patient();
        patient1.setFname("Ernest");
        patient1.setLname("Gayyed");
        patient1.setImageUrl("https://pbs.twimg.com/profile_images/761739039489667073/FoVG4Fu-_400x400.jpg");

        Patient patient2 = new Patient();
        patient2.setFname("Derrick");
        patient2.setLname("Rose");
        patient2.setImageUrl("http://a.espncdn.com/combiner/i?img=/i/headshots/nba/players/full/3456.png&w=350&h=254");

        Patient patient3 = new Patient();
        patient3.setFname("Kobe");
        patient3.setLname("Bryant");
        patient3.setImageUrl("https://b.fssta.com/uploads/application/nba/players/3100.vresize.350.425.medium.46.png");

        ConsultItem consultRequest1 = new ConsultItem();
        consultRequest1.setConsultSchedule("Monday 1am - 3am");
        consultRequest1.setConsultType("Video Consult");
        consultRequest1.setId("1");
        consultRequest1.setPatient(patient1);

        ConsultItem consultRequest2 = new ConsultItem();
        consultRequest2.setConsultSchedule("11am - 6am");
        consultRequest2.setConsultType("Video Consult");
        consultRequest2.setId("2");
        consultRequest2.setPatient(patient2);

        ConsultItem consultRequest3 = new ConsultItem();
        consultRequest3.setConsultSchedule("6am - 9am");
        consultRequest3.setConsultType("Video Consult");
        consultRequest3.setId("3");
        consultRequest3.setPatient(patient3);

        List<ConsultItem> consultRequests = new ArrayList<>();
        consultRequests.add(consultRequest1);
        consultRequests.add(consultRequest2);

        view.loadConsultSchedules(consultRequests);
    }
}
